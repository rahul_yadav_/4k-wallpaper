package com.appszum.wallpaper.hd.fragment

import android.app.ActivityOptions
import android.app.SearchManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.appszum.lib.api.OnSuccessListener
import com.appszum.lib.dialog.AlertView
import com.appszum.lib.fragment.TapFragment
import com.appszum.lib.listener.RecyclerListener
import com.appszum.lib.listener.RecyclerViewListener
import com.appszum.lib.utils.ImageCache
import com.appszum.lib.view.RecycleViewHolder
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.WallpaperListActivity
import com.appszum.wallpaper.hd.adapter.CategoryAdapter
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.SyncCategory
import com.appszum.wallpaper.hd.module.Category

class WallpaperTabFragment : TapFragment(), CategoryAdapter.AttachListener {

    companion object {
        @JvmStatic
        fun newInstance(cache: ImageCache, vararg tobs: Api.WallpaperList.TopColumn) =
                WallpaperTabFragment().apply {
                    tabList.clear()
                    list.clear()
                    tobs.forEach { item: Api.WallpaperList.TopColumn ->
                        tabList.add(item.title)
                        list.add(item)
                    }
                    if (tabList.isEmpty()) {
                        tabList.add("ALL")
                        tabList.add("TOP 20")
                    }
                    imageCache = cache
                    setHasOptionsMenu(list.isEmpty())
                }
    }

    lateinit var imageCache: ImageCache

    override val tabList: MutableList<String> = ArrayList()

    var list: ArrayList<Api.WallpaperList.TopColumn> = ArrayList()

    override var listener: RecyclerViewListener<Category> = object : RecyclerListener<Category>() {
        override fun onItemClick(holder: RecycleViewHolder<Category>, value: Category) {
            super.onItemClick(holder, value)
            val context = context ?: return
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivityForResult(WallpaperListActivity.intent(context, value), 100, ActivityOptions.makeSceneTransitionAnimation(activity, holder.itemView, getString(R.string.app_home)).toBundle())
            } else {
                startActivityForResult(WallpaperListActivity.intent(context, value), 100)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (list.isEmpty()) {
            SyncCategory(context!!, object : OnSuccessListener {
                override fun success() {
                    adapter?.fragments?.forEach { fragment ->
                        (fragment as? CategoryFragment)?.loadCategory()
                    }
                }

                override fun error() {

                }
            }).sync()
        }
    }


    override fun search(query: String) {
        (adapter?.current as? CategoryFragment)?.filter = query
    }

    override fun newInstance(position: Int): Fragment {
        if (!list.isEmpty()) {
            val fragment = WallpaperListFragment.newInstance(list[position])
            return fragment
        }
        val fragment = CategoryFragment.newInstance(this, position)
        fragment.imageCache = imageCache
        return fragment
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(com.appszum.lib.R.menu.navigation, menu)

        val searchView = menu?.findItem(com.appszum.lib.R.id.search)?.actionView as SearchView
        val searchManager = context?.getSystemService(AppCompatActivity.SEARCH_SERVICE) as SearchManager
        searchView.queryHint = getString(com.appszum.lib.R.string.search)

        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(p0: String?): Boolean {
                // search(p0 ?: "")
                return true
            }

            override fun onQueryTextSubmit(p0: String?): Boolean {
                //search(p0 ?: "")
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val adapter: CategoryAdapter? = (adapter?.first as? CategoryFragment)?.categoryAdapter
        val setting = adapter?.setting

        if (item != null) {
            when (item.itemId) {
                R.id.sort_by -> {
                    AlertView.popUpDialog<String>(context!!, title = getString(R.string.sort_by), values = resources.getStringArray(R.array.sort_by_list), select = setting?.sortBy
                            ?: 0, action = { i: Int, _: String? ->
                        setting?.sortBy = i
                        adapter?.notifyDataChanged()
                    }).show()
                }

                R.id.order_by -> {
                    AlertView.popUpDialog<String>(context!!, title = getString(R.string.order_by), values = resources.getStringArray(R.array.order_by_list), select = if (setting?.ascending == true) 0 else 1, action = { i: Int, _: String? ->
                        setting?.ascending = i == 0
                        adapter?.notifyDataChanged()
                    }).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        val adapter: CategoryAdapter? = (adapter?.first as? CategoryFragment)?.categoryAdapter
//        val setting = adapter?.setting
//
//        if (item != null) {
//            when (item.itemId) {
//                R.id.sort_by -> {
//                    AlertView.popUpDialog<String>(this, title = getString(R.string.sort_by), values = resources.getStringArray(R.array.sort_by_list), select = setting?.sortBy
//                            ?: 0, action = { i: Int, _: String? ->
//                        setting?.sortBy = i
//                        adapter?.notifyDataChanged()
//                    }).show()
//                }
//
//                R.id.order_by -> {
//                    AlertView.popUpDialog<String>(this, title = getString(R.string.order_by), values = resources.getStringArray(R.array.order_by_list), select = if (setting?.ascending == true) 0 else 1, action = { i: Int, _: String? ->
//                        setting?.ascending = i == 0
//                        adapter?.notifyDataChanged()
//                    }).show()
//                }
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }
}