package com.appszum.wallpaper.hd

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.android.volley.VolleyError
import com.appszum.lib.task.ImageLoadListener
import com.appszum.lib.task.TaskHandler
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.WallpaperResponse
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.github.chrisbanes.photoview.PhotoView
import com.google.gson.Gson
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_wallpaper_tabs.*
import kotlinx.android.synthetic.main.fragment_wallpaper_tabs.view.*
import org.json.JSONObject


class WallpaperPagerActivity : BaseInfoActivity(), com.appszum.lib.api.Api.VolleyListener<JSONObject>, TaskHandler {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null


    var found: Int = -1

    var top: Api.WallpaperList.TopColumn? = null

    var currentItem: Int
        get() = viewPager.currentItem
        set(value) {
            viewPager.currentItem = value
            ivNext.visibility = if (mSectionsPagerAdapter?.hasNext(value) == true) View.VISIBLE else View.GONE
            ivPrevious.visibility = if (value > 0) View.VISIBLE else View.GONE
        }

    lateinit var viewPager: ViewPager


    private var _4k: Boolean
        get() = setting._4K
        set(_4k) {
            setting._4K = _4k
        }


    companion object {
        fun intent(context: Context, category: Category, wallpaperInfo: WallpaperInfo): Intent {
            val intent = Intent(context, WallpaperPagerActivity::class.java)
            return wallpaperInfo.intent(category.intent(intent))
        }

        fun intent(context: Context, top: Api.WallpaperList.TopColumn, wallpaperInfo: WallpaperInfo): Intent {
            val intent = Intent(context, WallpaperPagerActivity::class.java)
            return wallpaperInfo.intent(top.intent(intent))
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallpaper_tabs)
        viewPager = findViewById(R.id.container)
        setSupportActionBar(toolbar)

        top = Api.WallpaperList.TopColumn.get(intent)
        if (category == null && top == null) {
            finish()
            return
        }

        title = if (category != null) category?.name else top?.title

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        if (wallpaperInfo != null) {
            mSectionsPagerAdapter?.list?.add(wallpaperInfo!!)
        }
        // Set up the ViewPager with the sections adapter.

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                val item = mSectionsPagerAdapter?.list?.get(position)
                if (item != null) {
                    fromDb(item) {
                        onSelect(item)
                    }
                }
            }

        })
        viewPager.adapter = mSectionsPagerAdapter
        if (wallpaperInfo != null) {
            fromDb(wallpaperInfo!!) {
                onSelect(it)
            }
            Api.WallpaperList.view(wallpaperInfo!!, listener = object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
                override fun onErrorResponse(error: VolleyError) {
                    print(error)
                }

                override fun onResponse(response: JSONObject) {
                    print(response)
                }
            }).call(this)
        }

        iv4K.setImageResource(if (_4k) R.drawable.ic_4k_on else R.drawable.ic_4k_off)

        if (category == null && top == null) {
            return
        }

        if (category != null) {
            callApi(category!!)
        } else if (top != null) {
            callApi(top!!)
        }
    }

    private fun callApi(category: Category) {
        Api.WallpaperList.allItems(category, listener = this).call(this)
    }

    private fun callApi(top: Api.WallpaperList.TopColumn) {
        if (top.fromDb) {
            task {
                val list = top.get(db.wallpaperInfoDao())
                this@WallpaperPagerActivity.ui {
                    show(list)
                }
            }
            return
        }
        Api.WallpaperList.top(top, this).call(this)
    }


    override fun onSelect(wallpaperInfo: WallpaperInfo) {
        super.onSelect(wallpaperInfo)
        ivNext.visibility = if (mSectionsPagerAdapter?.hasNext(currentItem) == true) View.VISIBLE else View.GONE
        ivPrevious.visibility = if (currentItem > 0) View.VISIBLE else View.GONE
    }


    override fun onResponse(response: JSONObject) {
        Log.d("response", response.toString())
        val r: WallpaperResponse = Gson().fromJson(response.toString(), WallpaperResponse::class.java)
        val result = r.response
        found = result?.found ?: 0

        task {
            val list: ArrayList<WallpaperInfo> = arrayListOf()

            if (result != null) {
                result.records.forEach { item: WallpaperInfo ->
                    val wallpaperInfo = db.wallpaperInfoDao().info(item.key, item.catkey) ?: item
                    val like = if (likes.has(item.key)) likes.getBoolean(item.key) else null
                    if (like != null) {
                        wallpaperInfo.liked = like
                        wallpaperInfo.disliked = !like
                    }
                    list.add(wallpaperInfo)
                }
            }

            ui {
                show(list)
            }
        }

        show(result?.records ?: arrayListOf())
    }

    fun show(list: List<WallpaperInfo>) {
        if (mSectionsPagerAdapter != null) {
            with(mSectionsPagerAdapter!!) {
                val wallpaperInfo = this@WallpaperPagerActivity.wallpaperInfo
                if (wallpaperInfo != null) {
                    this.list.addAll(list.filter { info -> info.key != wallpaperInfo.key })
                } else {
                    this.list.addAll(list)
                }
                ivNext.visibility = if (mSectionsPagerAdapter?.hasNext(currentItem) == true) View.VISIBLE else View.GONE
                ivPrevious.visibility = if (currentItem > 0) View.VISIBLE else View.GONE
                notifyDataSetChanged()
            }
        }
    }


    override fun onErrorResponse(error: VolleyError) {

    }

    override fun share(view: View) {
        super.share(view)
    }

    override fun favorite(view: View) {
        super.favorite(view)
    }

    override fun like(view: View) {
        super.like(view)
    }

    override fun dislike(view: View) {
        super.dislike(view)
    }

    override fun download(view: View) {
        super.download(view)
    }

    override fun shareOnFacebook(view: View?) {
        super.shareOnFacebook(view)
    }

    override fun setWallpaper(view: View) {
        super.setWallpaper(view)
    }

    fun info(view: View) {
        info(selected)
    }

    override fun info(wallpaperInfo: WallpaperInfo) {
        startActivity(WallPaperInfoActivity.intent(this, wallpaperInfo))
    }

    fun next(view: View) {
        if (mSectionsPagerAdapter?.hasNext(currentItem) == true) {
            currentItem++
        }
    }

    fun previous(view: View) {
        if (currentItem > 0) {
            currentItem--
        }
    }


    fun show4K(view: View) {
        _4k = !_4k
        anim(view, _4k)
        iv4K.setImageResource(if (_4k) R.drawable.ic_4k_on else R.drawable.ic_4k_off)
    }

    override val selected: WallpaperInfo get() = mSectionsPagerAdapter!!.list[currentItem]


    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        val list: ArrayList<WallpaperInfo> = arrayListOf()

        override fun getItem(position: Int): Fragment {
            return PlaceholderFragment.newInstance(list[position], _4k, imageLoad)
        }

        override fun getCount(): Int {
            return list.count()
        }

        fun hasNext(currentPosition: Int): Boolean {
            return count > (currentPosition + 1)
        }
    }

    class PlaceholderFragment : Fragment() {


        lateinit var wallpaperInfo: WallpaperInfo

        lateinit var imageView: PhotoView
        lateinit var ivLoading: ImageView
        lateinit var imageLoadListener: ImageLoadListener<WallpaperInfo>


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val rootView = inflater.inflate(R.layout.fragment_wallpaper_tabs, container, false)

            wallpaperInfo = WallpaperInfo.get(arguments!!)!!
            imageView = rootView.imageView
            imageView.scale = 1f
            ivLoading = rootView.ivLoading
            var _4k = false
            if (arguments != null && arguments!!.containsKey("4K")) {
                _4k = arguments!!.getBoolean("4K")
            }
            loadImage(_4k)

            return rootView
        }

        private fun loadImage(_4k: Boolean) {
            imageLoadListener.load(wallpaperInfo) {
                if (it != null) {
                    imageView.visibility = View.VISIBLE
                    ivLoading.visibility = View.GONE
                    imageView.setImageBitmap(it)
                    return@load
                }
                imageView.visibility = View.GONE
                ivLoading.visibility = View.VISIBLE
                Picasso.get()
                        .load(if (_4k) wallpaperInfo._4kUrl else wallpaperInfo.imagePath)
                        .into(imageView, object : Callback {
                            override fun onSuccess() {
                                imageView.visibility = View.VISIBLE
                                ivLoading.visibility = View.GONE
                            }

                            override fun onError(e: java.lang.Exception?) {

                            }
                        })

            }
        }


        companion object {
            fun newInstance(wallpaperInfo: WallpaperInfo, _4k: Boolean, imageLoadListener: ImageLoadListener<WallpaperInfo>): PlaceholderFragment {
                return PlaceholderFragment().apply {
                    this.imageLoadListener = imageLoadListener
                    val args = wallpaperInfo.bundle()
                    args.putBoolean("4K", _4k)
                    this.arguments = args
                }
            }
        }
    }
}
