package com.appszum.wallpaper.hd

import android.view.View

class LoginActivity : com.appszum.lib.LoginActivity() {
    override val icon: Int
        get() = R.drawable.web_hi_res_512


    override fun facebookLogin(view: View) {
        super.facebookLogin(view)
    }

    override fun twitterLogin(view: View) {
        super.twitterLogin(view)
    }


    override fun onStart() {
        super.onStart()
        val user = loginService.user
        if (user != null) {
            loggedIn(user)
        }
    }


}
