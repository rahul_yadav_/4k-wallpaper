package com.appszum.wallpaper.hd.module

import android.arch.persistence.room.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.appszum.lib.toByteArray


@Entity(tableName = "ImageInfoTable",
        indices = [Index(value = ["key", "path"], unique = true)])

class ImageInfo {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    lateinit var key: String
    lateinit var path: String
    var type: Int = 0
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    lateinit var image: ByteArray

    companion object {
        fun get(category: Category, bitmap: Bitmap): ImageInfo {
            val imageInfo = ImageInfo()
            imageInfo.key = category.key
            imageInfo.path = category.path
            imageInfo.image = bitmap.toByteArray()
            imageInfo.type = 1
            return imageInfo
        }

        fun get(wallpaperInfo: WallpaperInfo, bitmap: Bitmap): ImageInfo {
            val imageInfo = ImageInfo()
            imageInfo.key = wallpaperInfo.key
            imageInfo.path = wallpaperInfo.image
            imageInfo.image = bitmap.toByteArray()
            imageInfo.type = 2
            return imageInfo
        }
    }

    fun isSameUrl(wallpaperInfo: WallpaperInfo): Boolean {
        return wallpaperInfo.key == key && wallpaperInfo.image == path
    }

    fun isSameUrl(category: Category): Boolean {
        return category.key == key && category.path == path
    }


    var bitmap: Bitmap
        get() {
            return BitmapFactory.decodeByteArray(image, 0, image.size)
        }
        set(value) {
            image = value.toByteArray()
        }


}


@Dao
interface ImageInfoDao {

    @Query("SELECT * from ImageInfoTable WHERE `type`= :type")
    fun get(type: Int): List<ImageInfo>?

    @Query("SELECT * from ImageInfoTable WHERE `key`= :key AND `path`= :path")
    fun info(key: String, path: String): ImageInfo?

    @Query("SELECT * from ImageInfoTable WHERE `key`= :key AND `type`= :type")
    fun info(key: String, type: Int): ImageInfo?

    @Query("SELECT * from ImageInfoTable WHERE `id`= :id")
    fun info(id: Long): ImageInfo?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(imageInfo: ImageInfo): Long

    @Query("DELETE from ImageInfoTable WHERE `id`= :id")
    fun remove(id: Long)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(imageInfo: ImageInfo)

}