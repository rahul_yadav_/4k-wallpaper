package com.appszum.wallpaper.hd

import android.Manifest
import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.view.animation.AnimationUtils
import com.android.volley.VolleyError
import com.appszum.lib.BaseActivity
import com.appszum.lib.dialog.AlertView
import com.appszum.lib.error
import com.appszum.lib.load
import com.appszum.lib.task.DbWorkerThread
import com.appszum.lib.task.DownloadListener
import com.appszum.lib.task.TaskHandler
import com.appszum.lib.toast
import com.appszum.lib.utils.Utility
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.WallpaperResponse
import com.appszum.wallpaper.hd.dialog.DownloadDialog
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperDatabase
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.appszum.wallpaper.hd.settings.UserSetting
import com.appszum.wallpaper.hd.task.BitmapDownloadTask
import com.appszum.wallpaper.hd.task.WallpaperImageLoad
import com.google.android.gms.ads.InterstitialAd
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_wallpaper_tabs.*
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


abstract class BaseInfoActivity : BaseActivity(), TaskHandler {

    open var wallpaperInfo: WallpaperInfo? = null
        set(wallpaperInfo) {
            field = wallpaperInfo
            if (wallpaperInfo != null) {
                title = wallpaperInfo.name

                fromDb(wallpaperInfo) {
                    onSelect(it)
                }
                Api.WallpaperList.view(wallpaperInfo, listener = object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
                    override fun onErrorResponse(error: VolleyError) {
                        application.error(error)
                    }

                    override fun onResponse(response: JSONObject) {
                        print(response)
                        val r: WallpaperResponse = Gson().fromJson(response.toString(), WallpaperResponse::class.java)
                        val result = r.response ?: return
                        val category = result.categories.find { category -> wallpaperInfo.catkey == category.key }
                        if (category != null) {
                            categoryFound(category)
                        }
                    }
                }).call(this)
            }
        }

    var category: Category? = null

    lateinit var setting: UserSetting
    lateinit var db: WallpaperDatabase
    var likes: JSONObject = JSONObject()

    private lateinit var mDbWorkerThread: DbWorkerThread

    lateinit var imageLoad: WallpaperImageLoad

    open fun categoryFound(category: Category) {

    }

    companion object {
        fun intent(context: Context, category: Category, wallpaperInfo: WallpaperInfo): Intent {
            val intent = Intent(context, BaseInfoActivity::class.java)
            return wallpaperInfo.intent(category.intent(intent))
        }

        fun intent(context: Context, top: Api.WallpaperList.TopColumn, wallpaperInfo: WallpaperInfo): Intent {
            val intent = Intent(context, BaseInfoActivity::class.java)
            return wallpaperInfo.intent(top.intent(intent))
        }
    }

    var mInterstitialAd: InterstitialAd? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        super.onCreate(savedInstanceState)
        setting = UserSetting(this)
        likes = setting.likes

        mInterstitialAd = com.appszum.lib.InterstitialAd.get(this)

        db = WallpaperDatabase.getInstance(this)!!
        imageLoad = WallpaperImageLoad(db)

    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        category = Category.get(intent)
        wallpaperInfo = WallpaperInfo.get(intent)
    }


    fun save(wallpaperInfo: WallpaperInfo) {
        task {
            val dao = db.wallpaperInfoDao()
            if (wallpaperInfo.favorite) {
                wallpaperInfo.update(dao)
                toast(getString(R.string.favorite_added))
            } else {
                if (wallpaperInfo.dowloaded) {
                    wallpaperInfo.update(dao)
                } else {
                    dao.remove(wallpaperInfo.key, wallpaperInfo.catkey)
                }
                toast(getString(R.string.favorite_remove))
            }
            ui {
                onSelect(wallpaperInfo)
            }
        }
    }

    fun download(wallpaperInfo: WallpaperInfo, bitmap: Bitmap, onSave: (Boolean) -> Unit) {
        imageLoad.save(wallpaperInfo, bitmap) {
            toast(getString(if (it) R.string.successfully_downloaded else R.string.problem_with_image_downloading))
            onSave(it)
            onSelect(wallpaperInfo)
        }
    }


    private fun removeDownload(wallpaperInfo: WallpaperInfo) {
        AlertView.showActionDialog(this, message = "Do you want to remove wallpaper from database") { yes ->
            if (yes) {
                task {
                    val imageId = wallpaperInfo.imageId
                    if (imageId != null) {
                        db.imageInfoDao().remove(imageId)
                        wallpaperInfo.imageId = null
                        if (wallpaperInfo.favorite) {
                            db.wallpaperInfoDao().update(wallpaperInfo)
                        } else {
                            db.wallpaperInfoDao().remove(wallpaperInfo.key, wallpaperInfo.catkey)
                        }
                        toast(getString(R.string.download_remove))
                    }

                    ui {
                        onSelect(wallpaperInfo)
                    }
                }
            }
        }.show()
    }


    fun fromDb(item: WallpaperInfo, found: (WallpaperInfo) -> Unit) {
        task {
            val wallpaperInfo = db.wallpaperInfoDao().info(item.key, item.catkey) ?: item
            val like = if (likes.has(item.key)) likes.getBoolean(item.key) else null
            if (like != null) {
                wallpaperInfo.liked = like
                wallpaperInfo.disliked = !like
            }
            ui {
                found(wallpaperInfo)
            }
        }
    }


    override fun task(action: () -> Unit) {
        mDbWorkerThread.task(action)
    }

    override fun ui(action: () -> Unit) {
        mDbWorkerThread.ui(action)
    }


    open fun onSelect(wallpaperInfo: WallpaperInfo) {
        ivLike?.setImageResource(if (wallpaperInfo.liked) R.drawable.like_on else R.drawable.like_off)
        ivDisLike?.setImageResource(if (wallpaperInfo.disliked) R.drawable.dislike_on else R.drawable.dislike_off)
        ivDownload?.setImageResource(if (wallpaperInfo.dowloaded) R.drawable.download_on else R.drawable.download_off)
        ivFavorite?.setImageResource(if (wallpaperInfo.favorite) R.drawable.favorite_on else R.drawable.favorite_off)
    }

    open fun like(view: View) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.click_aim))
        val item = selected
        var disliked: Boolean? = null
        if (item.disliked) {
            disliked = true
        }

        item.liked = !item.liked
        anim(view, item.liked)
        likes = setting.like(item)
        onSelect(item)
        if (item.liked) {
            toast(getString(R.string.like_wallpaper))
        }
        Api.WallpaperList.likes(item, item.liked, disliked, listener = object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
            override fun onErrorResponse(error: VolleyError) {
                print(error)
            }

            override fun onResponse(response: JSONObject) {
                print(response)
            }
        }).call(this)
        mInterstitialAd?.load(this)
    }

    open fun dislike(view: View) {
        val item = selected
        var liked: Boolean? = null
        if (item.liked) {
            liked = true
        }
        item.disliked = !item.disliked
        anim(view, item.disliked)
        likes = setting.like(item)
        onSelect(item)
        if (item.disliked) {
            toast(getString(R.string.dislike_wallpaper))
        }
        Api.WallpaperList.likes(item, liked, item.disliked, listener = object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
            override fun onErrorResponse(error: VolleyError) {
                print(error)
            }

            override fun onResponse(response: JSONObject) {
                print(response)
            }
        }).call(this)
        mInterstitialAd?.load(this)
    }

    open fun favorite(view: View) {
        favorite(selected, view)
    }

    override fun onResume() {
        super.onResume()
        onSelect(selected)
        invalidateOptionsMenu()
    }

    open fun favorite(wallpaperInfo: WallpaperInfo, view: View? = null) {

        wallpaperInfo.favorite = !wallpaperInfo.favorite
        if (view != null) {
            anim(view, wallpaperInfo.favorite)
        }

        save(wallpaperInfo)
        mInterstitialAd?.load(this)
    }

    open fun download(view: View) {
        download(selected, view)
    }

    open fun download(wallpaperInfo: WallpaperInfo, view: View? = null) {

        if (wallpaperInfo.dowloaded) {
            removeDownload(wallpaperInfo)
            return
        }
        DownloadDialog.show(this, wallpaperInfo, object : DownloadDialog.Listener {
            override fun info(wallpaperInfo: WallpaperInfo) {
                this@BaseInfoActivity.info(wallpaperInfo)
            }

            override fun setWallpaper(wallpaperInfo: WallpaperInfo) {
                this@BaseInfoActivity.setWallpaper(wallpaperInfo)
            }

            override fun save(wallpaperInfo: WallpaperInfo, bitmap: Bitmap, onSave: (Boolean) -> Unit) {
                download(wallpaperInfo, bitmap) {
                    if (view != null && it) {
                        anim(view, wallpaperInfo.dowloaded)
                    }
                    onSave(it)
                }
            }

        })
    }

    abstract fun info(wallpaperInfo: WallpaperInfo)

    open fun setWallpaper(view: View) {
        setWallpaper(selected)
    }


    fun setWallpaper(wallpaperInfo: WallpaperInfo) {
/*
        try {
            val intent = Intent(Intent.ACTION_SET_WALLPAPER)
            //intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            startActivity(intent)
            return
        }catch (ex: Exception){

        }*/

        AlertView.popUpDialog<String>(this, getString(R.string.set_wallpaper), arrayOf(getString(R.string.home_screen), getString(R.string.lock_screen), getString(R.string.home_and_lock_screen)), 0)
        { i: Int, _: String? ->
            load4KImage(wallpaperInfo) {
                if (it != null) {
                    showProgressAlert("setting wallpaper....")
                    setWallpaper(it, which = i) {
                        dismissProgressAlert()
                    }
                    return@load4KImage
                }
            }
        }

    }

    fun load4KImage(wallpaperInfo: WallpaperInfo, action: (Bitmap?) -> Unit) {
        imageLoad.load(wallpaperInfo) {
            if (it != null) {
                action(it)
                return@load
            }
            showProgressAlert("loading 4k image...")

            BitmapDownloadTask(wallpaperInfo, object : DownloadListener<Bitmap> {
                override fun success(result: Bitmap) {
                    action(result)
                    dismissProgressAlert()
                }

                override fun onProgressUpdate(progress: Float) {
                    showProgressAlert("${String.format("%.2f", progress)}% loading 4k image...")
                }

                override fun error() {
                    toast("problem in image loading!!")
                    action(null)
                    dismissProgressAlert()
                }
            }).execute()
        }
    }

    private fun setWallpaper(bitmap: Bitmap, which: Int, action: () -> Unit) {
        task {
            val wallpaperManager = WallpaperManager.getInstance(applicationContext)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                var flags = WallpaperManager.FLAG_SYSTEM
                if (which == 1) {
                    flags = WallpaperManager.FLAG_LOCK
                } else if (which == 2) {
                    flags = WallpaperManager.FLAG_SYSTEM or WallpaperManager.FLAG_SYSTEM
                }
                wallpaperManager.setBitmap(bitmap, Rect(0, 0, bitmap.width, bitmap.height), false, flags)
            } else {
                wallpaperManager.setBitmap(bitmap)
            }
            ui {
                toast("Wallpaper set successfully!!")
                action()
            }
        }

    }

    open fun saveFile(view: View) {
        saveFile(selected)
    }

    private fun hasPermission(permission: String): Boolean {
        return checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }


    fun saveFile(wallpaperInfo: WallpaperInfo) {
        val permissions = arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(permissions, 100)
            return
        }

        load4KImage(wallpaperInfo) { image ->
            dismissProgressAlert()
            if (image != null) {
                showProgressAlert("saving file into storage!!")
                task {
                    // Assume block needs to be inside a Try/Catch block.
                    var fOut: OutputStream? = null
                    val dir = File(Environment.getExternalStorageDirectory().absolutePath + "/Wallpaper 4k")
                    if (!dir.exists()) {
                        dir.mkdir()
                    }
                    val file = File(dir, "${wallpaperInfo.name}.jpeg") // the File to save , append increasing numeric counter to prevent files from getting overwritten.
                    fOut = FileOutputStream(file)

                    image.compress(Bitmap.CompressFormat.JPEG, 100, fOut) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                    fOut.flush() // Not really required
                    fOut.close() // do not forget to close the stream

                    val path = MediaStore.Images.Media.insertImage(contentResolver, file.absolutePath, file.name, file.name)
                    ui {
                        toast("File successfully saved into storage!!")
                        dismissProgressAlert()
                        Utility.Launch.shareFile(this, path)
                        mInterstitialAd?.load(this)
                    }
                }

            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            100 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveFile(selected)
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    fun anim(view: View, animation: Boolean) {
        if (animation) {
            view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.click_aim))
        }
    }

    abstract val selected: WallpaperInfo


    open fun share(view: View) {
        share(selected)
    }

    fun share(wallpaperInfo: WallpaperInfo) {
        val permissions = arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            requestPermissions(permissions, 110)
            return
        }
        load4KImage(wallpaperInfo) { image ->
            if (image != null) {
                Utility.Launch.shareImage(this, image) {
                    mInterstitialAd?.load(this)
                }
            }
        }
    }

    open fun shareOnFacebook(view: View? = null) {
        load4KImage(selected) { image ->
            if (image != null) {
                Utility.Launch.shareOnFacebook(this, image)
                mInterstitialAd?.load(this)
            }
        }
    }


}
