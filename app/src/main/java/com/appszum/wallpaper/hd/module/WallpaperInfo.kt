package com.appszum.wallpaper.hd.module

import android.arch.persistence.room.*
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import com.appszum.lib.syncFormat
import com.appszum.wallpaper.hd.api.Api
import org.json.JSONObject
import java.io.Serializable
import java.util.*

@Entity(tableName = "WallpaperInfoTable",
        indices = [Index(value = ["key", "catkey"], unique = true)],
        foreignKeys = [ForeignKey(entity = Category::class, parentColumns = ["key"], childColumns = ["catkey"]),
            ForeignKey(entity = ImageInfo::class, parentColumns = ["id"], childColumns = ["imageId"], onDelete = ForeignKey.SET_DEFAULT)])
class WallpaperInfo : Serializable {
    @ColumnInfo
    lateinit var name: String

    @PrimaryKey
    @ColumnInfo
    lateinit var key: String
    @ColumnInfo
    lateinit var catkey: String
    @ColumnInfo
    var size: Long = 0
    @ColumnInfo
    var download: Int = 0
    @ColumnInfo
    var views: Int = 0
    @ColumnInfo
    var likes: Int = 0
    @ColumnInfo
    var dislikes: Int = 0
    @ColumnInfo
    var width: Int = 0
    @ColumnInfo
    var height: Int = 0
    @ColumnInfo
    lateinit var image: String

    @ColumnInfo
    lateinit var icon: String

    @ColumnInfo
    var exist: Boolean = false

    @ColumnInfo
    var updated: String = Date().syncFormat()

    //local
    @Ignore
    var bitmap: Bitmap? = null


    var liked: Boolean
        get() {
            return _like == 1
        }
        set(like) {
            if (disliked) {
                dislikes--
            }
            _like = if (like) 1 else 0
            if (like) {
                this.likes++
            } else {
                this.likes--
            }
        }

    var disliked: Boolean
        get() {
            return _like == -1
        }
        set(dislike) {
            _like = if (dislike) -1 else 0
            if (liked) {
                likes--
            }
            if (dislike) {
                this.dislikes++
            } else {
                this.dislikes--
            }
        }

    @Ignore
    var _like: Int = 0

    @ColumnInfo
    var favorite: Boolean = false

    @ColumnInfo()
    var imageId: Long? = null


    val dowloaded: Boolean get() = imageId != null

    val thumbnails: String get() = "${Api.HOST}/${image}&width=512"

    val imagePath: String get() = "${Api.HOST}/${image}&width=1024"

    val _4kUrl: String get() = "${Api.HOST}/${image}"

    val _fileUrl: String get() = _4kUrl

    val json: JSONObject
        get() = JSONObject().apply {
            put("name", name)
            put("key", key)
            put("catkey", catkey)
            put("size", size)
            put("download", download)
            put("views", views)

            put("likes", likes)
            put("dislikes", dislikes)
            put("width", width)
            put("height", height)
            put("image", image)

            put("icon", icon)
        }


    val sizeInfo: String
        get() {
            if (size < 1024) {
                return "${size} byte"
            }
            val kb = size.toFloat() / 1024;

            if (kb < 1024) {
                return "${String.format("%.2f", kb)} KB"
            }

            val mb = kb / 1024;

            if (mb < 1024) {
                return "${String.format("%.2f", mb)} MB"
            }

            return "${String.format("%.2f", mb / 1024)} GB"

        }

    fun intent(intent: Intent): Intent {
        intent.putExtra("WallpaperInfo", this)
        return intent
    }

    fun bundle(bundle: Bundle = Bundle()): Bundle {
        bundle.putSerializable("WallpaperInfo", this)
        return bundle
    }

    fun set(imageInfo: ImageInfo) {
        imageId = imageInfo.id
    }


    fun update(wallpaperInfoDao: WallpaperInfoDao) {
        if (!exist) {
            insert(wallpaperInfoDao)
            return
        }
        updated = Date().syncFormat()
        wallpaperInfoDao.update(this)
    }

    private fun insert(wallpaperInfoDao: WallpaperInfoDao) {
        exist = true
        updated = Date().syncFormat()
        wallpaperInfoDao.insert(this)
    }

    companion object {
        fun get(intent: Intent): WallpaperInfo? {
            return if (intent.hasExtra("WallpaperInfo")) intent.getSerializableExtra("WallpaperInfo") as WallpaperInfo else null
        }

        fun get(bundle: Bundle): WallpaperInfo? {
            return if (bundle.containsKey("WallpaperInfo")) bundle.getSerializable("WallpaperInfo") as WallpaperInfo else null
        }
    }
}

@Dao
interface WallpaperInfoDao {

    @Query("SELECT * from WallpaperInfoTable")
    fun getAll(): List<WallpaperInfo>

    @Query("SELECT * from WallpaperInfoTable  WHERE `favorite`= 1")
    fun allFavorite(): List<WallpaperInfo>

    @Query("SELECT * from WallpaperInfoTable  WHERE `imageId` > 0")
    fun allDownloaded(): List<WallpaperInfo>

    @Query("SELECT * from WallpaperInfoTable WHERE `key`= :key AND `catkey`= :catkey")
    fun info(key: String, catkey: String): WallpaperInfo?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weatherData: WallpaperInfo)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(weatherData: WallpaperInfo)

    @Query("DELETE from WallpaperInfoTable WHERE `key`= :key AND `catkey`= :catkey")
    fun remove(key: String, catkey: String)

    @Query("DELETE from WallpaperInfoTable")
    fun deleteAll()
}