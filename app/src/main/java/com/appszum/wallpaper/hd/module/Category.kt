package com.appszum.wallpaper.hd.module

import android.arch.persistence.room.*
import android.content.Intent
import android.os.Bundle
import com.appszum.lib.syncFormat
import com.appszum.wallpaper.hd.api.Api
import org.json.JSONObject
import java.io.Serializable
import java.util.*
import kotlin.Comparator


@Entity(tableName = "CategoryTable", primaryKeys = ["key"],
        foreignKeys = [ForeignKey(entity = ImageInfo::class, parentColumns = ["id"], childColumns = ["imageId"], onDelete = ForeignKey.SET_DEFAULT)])
class Category : Serializable, Comparable<Category> {

    @ColumnInfo
    lateinit var name: String
    @ColumnInfo
    lateinit var key: String
    @ColumnInfo
    var total: Int = 0
    @ColumnInfo
    lateinit var path: String
    @ColumnInfo
    lateinit var icon: String
    @Ignore
    var fImage: String? = null

    @ColumnInfo()
    var imageId: Long? = null


    val dowloaded: Boolean get() = imageId != null

    val thumbnails: String
        get() {
            return "${Api.HOST}/$icon&width=512"
        }

    @ColumnInfo
    var active: Boolean = true

    @ColumnInfo
    var exist: Boolean = false

    @ColumnInfo
    var updated: String = Date().syncFormat()


    val jsonObject: JSONObject
        get() = JSONObject().apply {
            put("name", name)
            put("key", key)
            put("total", total)
            put("path", path)
            put("icon", icon)
            put("fImage", fImage)
        }

    val map: HashMap<String, Any?>
        get() = HashMap<String, Any?>().apply {
            put("name", name)
            put("key", key)
            put("total", total)
            put("path", path)
            put("icon", icon)
            put("fImage", fImage)
            put("active", true)
            put("updated_at", Date())
        }

    override fun compareTo(other: Category): Int {
        return name.compareTo(other.name)
    }


    fun intent(intent: Intent): Intent {
        intent.putExtra("Category", this)
        return intent
    }

    val bundle: Bundle
        get() {
            return Bundle().apply {
                putSerializable("Category", this@Category)
            }
        }

    fun update(categoryDao: CategoryDao) {
        try {
            if (!exist) {
                insert(categoryDao)
                return
            }
            updated = Date().syncFormat()
            categoryDao.update(this)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun insert(categoryDao: CategoryDao) {
        try {
            exist = true
            updated = Date().syncFormat()
            categoryDao.insert(this)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    companion object {
        fun get(intent: Intent): Category? {
            return if (intent.hasExtra("Category")) intent.getSerializableExtra("Category") as Category else null
        }

        fun get(bundle: Bundle): Category? {
            return if (bundle.containsKey("Category")) bundle.getSerializable("Category") as Category else null
        }

        val NameAscending = Comparator<Category> { o1, o2 -> o1.name.compareTo(o2.name) }

        val NameDescending = Comparator<Category> { o1, o2 -> o2.name.compareTo(o1.name) }

        val DateAscending = Comparator<Category> { o1, o2 ->
            o1.updated.compareTo(o2.updated)
        }

        val DateDescending = Comparator<Category> { o1, o2 ->
            o2.updated.compareTo(o1.updated)
        }

        val TotalAscending = Comparator<Category> { o1, o2 -> o1.total - o2.total }

        val TotalDescending = Comparator<Category> { o1, o2 -> o2.total - o1.total }
    }
}

@Dao
interface CategoryDao {

    @Query("SELECT * from CategoryTable")
    fun getAll(): List<Category>

    @Query("SELECT * from CategoryTable WHERE `key`= :key")
    fun info(key: String): Category?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: Category)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(category: Category)

    @Query("DELETE from CategoryTable")
    fun deleteAll()
}