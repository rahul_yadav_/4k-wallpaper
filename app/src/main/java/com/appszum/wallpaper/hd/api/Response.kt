package com.appszum.wallpaper.hd.api

import com.appszum.lib.api.Response
import com.appszum.lib.api.ResponseResult
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperInfo


class CategoryResponse : Response<Category, CategoryResponse.Result>() {

    class Result : ResponseResult<Category>() {

    }
}


class WallpaperResponse : Response<WallpaperInfo, WallpaperResponse.Result>() {

    class Result : ResponseResult<WallpaperInfo>() {
        var categories = arrayListOf<Category>()
    }
}