/*
 * Copyright (c) 2012 Jason Polites
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appszum.wallpaper.hd.zoom

import android.content.res.Configuration
import android.graphics.PointF
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnClickListener
import android.view.View.OnTouchListener

class GestureImageViewTouchListener(private val image: GestureImageView, private val displayWidth: Int, private val displayHeight: Int) : OnTouchListener {

    private val current = PointF()
    private val last = PointF()
    private val next = PointF()
    private val midpoint = PointF()
    private val scaleVector = VectorF()
    private val pinchVector = VectorF()
    var onClickListener: OnClickListener? = null
    private var touched = false
    private var inZoom = false

    private var initialDistance: Float = 0.toFloat()
    private var lastScale = 1.0f
    private var currentScale = 1.0f

    private var boundaryLeft = 0f
    private var boundaryTop = 0f
    private var boundaryRight = 0f
    private var boundaryBottom = 0f

    var maxScale = 5.0f
    var minScale = 0.25f
    var fitScaleHorizontal = 1.0f
    var fitScaleVertical = 1.0f

    var canvasWidth = 0
    var canvasHeight = 0

    private val centerX = displayWidth.toFloat() / 2.0f
    private val centerY = displayHeight.toFloat() / 2.0f

    private var startingScale = 0f

    private var canDragX = false
    private var canDragY = false

    private var multiTouch = false

    private val imageWidth: Int = image.imageWidth
    private val imageHeight: Int = image.imageHeight

    private val flingListener: FlingListener
    private val flingAnimation: FlingAnimation
    private val zoomAnimation: ZoomAnimation
    private val moveAnimation: MoveAnimation
    private val tapDetector: GestureDetector
    private val flingDetector: GestureDetector
    private val imageListener: GestureImageViewListener?

    init {

        startingScale = image.scale

        currentScale = startingScale
        lastScale = startingScale

        boundaryRight = displayWidth.toFloat()
        boundaryBottom = displayHeight.toFloat()
        boundaryLeft = 0f
        boundaryTop = 0f

        next.x = image.imageX
        next.y = image.imageY

        flingListener = FlingListener()
        flingAnimation = FlingAnimation()
        zoomAnimation = ZoomAnimation()
        moveAnimation = MoveAnimation()

        flingAnimation.setListener(object : FlingAnimationListener {
            override fun onMove(x: Float, y: Float) {
                handleDrag(current.x + x, current.y + y)
            }

            override fun onComplete() {}
        })

        zoomAnimation.zoom = 2.0f
        zoomAnimation.zoomAnimationListener = object : ZoomAnimationListener {
            override fun onZoom(scale: Float, x: Float, y: Float) {
                if (scale <= maxScale && scale >= minScale) {
                    handleScale(scale, x, y)
                }
            }

            override fun onComplete() {
                inZoom = false
                handleUp()
            }
        }

        moveAnimation.setMoveAnimationListener(moveAnimationListener = object : MoveAnimationListener {
            override fun onMove(x: Float, y: Float) {
                image.setPosition(x, y)
                image.redraw()
            }
        })


        tapDetector = GestureDetector(image.context, object : SimpleOnGestureListener() {
            override fun onDoubleTap(e: MotionEvent): Boolean {
                startZoom(e)
                return true
            }

            override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
                if (!inZoom) {
                    if (onClickListener != null) {
                        onClickListener!!.onClick(image)
                        return true
                    }
                }

                return false
            }
        })

        flingDetector = GestureDetector(image.context, flingListener)
        imageListener = image.gestureImageViewListener

        calculateBoundaries()
    }

    private fun startFling() {
        flingAnimation.setVelocityX(flingListener.velocityX)
        flingAnimation.setVelocityY(flingListener.velocityY)
        image.animationStart(flingAnimation)
    }

    private fun startZoom(e: MotionEvent) {
        inZoom = true
        zoomAnimation.reset()

        val zoomTo: Float

        if (image.isLandscape) {
            if (image.deviceOrientation == Configuration.ORIENTATION_PORTRAIT) {
                val scaledHeight = image.scaledHeight

                if (scaledHeight < canvasHeight) {
                    zoomTo = fitScaleVertical / currentScale
                    zoomAnimation.touchX = e.x
                    zoomAnimation.touchY = image.centerY
                } else {
                    zoomTo = fitScaleHorizontal / currentScale
                    zoomAnimation.touchX = image.centerX
                    zoomAnimation.touchY = image.centerY
                }
            } else {
                val scaledWidth = image.scaledWidth

                if (scaledWidth == canvasWidth) {
                    zoomTo = currentScale * 4.0f
                    zoomAnimation.touchX = e.x
                    zoomAnimation.touchY = e.y
                } else if (scaledWidth < canvasWidth) {
                    zoomTo = fitScaleHorizontal / currentScale
                    zoomAnimation.touchX = image.centerX
                    zoomAnimation.touchY = e.y
                } else {
                    zoomTo = fitScaleHorizontal / currentScale
                    zoomAnimation.touchX = image.centerX
                    zoomAnimation.touchY = image.centerY
                }
            }
        } else {
            if (image.deviceOrientation == Configuration.ORIENTATION_PORTRAIT) {

                val scaledHeight = image.scaledHeight

                if (scaledHeight == canvasHeight) {
                    zoomTo = currentScale * 4.0f
                    zoomAnimation.touchX = e.x
                    zoomAnimation.touchY = e.y
                } else if (scaledHeight < canvasHeight) {
                    zoomTo = fitScaleVertical / currentScale
                    zoomAnimation.touchX = e.x
                    zoomAnimation.touchY = image.centerY
                } else {
                    zoomTo = fitScaleVertical / currentScale
                    zoomAnimation.touchX = image.centerX
                    zoomAnimation.touchY = image.centerY
                }
            } else {
                val scaledWidth = image.scaledWidth

                if (scaledWidth < canvasWidth) {
                    zoomTo = fitScaleHorizontal / currentScale
                    zoomAnimation.touchX = image.centerX
                    zoomAnimation.touchY = e.y
                } else {
                    zoomTo = fitScaleVertical / currentScale
                    zoomAnimation.touchX = image.centerX
                    zoomAnimation.touchY = image.centerY
                }
            }
        }

        zoomAnimation.zoom = zoomTo
        image.animationStart(zoomAnimation)
    }


    private fun stopAnimations() {
        image.animationStop()
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {

        if (!inZoom) {

            if (!tapDetector.onTouchEvent(event)) {
                if (event.pointerCount == 1 && flingDetector.onTouchEvent(event)) {
                    startFling()
                }

                if (event.action == MotionEvent.ACTION_UP) {
                    handleUp()
                } else if (event.action == MotionEvent.ACTION_DOWN) {
                    stopAnimations()

                    last.x = event.x
                    last.y = event.y

                    imageListener?.onTouch(last.x, last.y)

                    touched = true
                } else if (event.action == MotionEvent.ACTION_MOVE) {
                    if (event.pointerCount > 1) {
                        multiTouch = true
                        if (initialDistance > 0) {

                            pinchVector.set(event)
                            pinchVector.calculateLength()

                            val distance = pinchVector.length

                            if (initialDistance != distance) {

                                val newScale = distance / initialDistance * lastScale

                                if (newScale <= maxScale) {
                                    scaleVector.length *= newScale

                                    scaleVector.calculateEndPoint()

                                    scaleVector.length /= newScale

                                    val newX = scaleVector.end.x
                                    val newY = scaleVector.end.y

                                    handleScale(newScale, newX, newY)
                                }
                            }
                        } else {
                            initialDistance = MathUtils.distance(event)

                            MathUtils.midpoint(event, midpoint)

                            scaleVector.setStart(midpoint)
                            scaleVector.setEnd(next)

                            scaleVector.calculateLength()
                            scaleVector.calculateAngle()

                            scaleVector.length /= lastScale
                        }
                    } else {
                        if (!touched) {
                            touched = true
                            last.x = event.x
                            last.y = event.y
                            next.x = image.imageX
                            next.y = image.imageY
                        } else if (!multiTouch) {
                            if (handleDrag(event.x, event.y)) {
                                image.redraw()
                            }
                        }
                    }
                }
            }
        }

        return true
    }

    protected fun handleUp() {

        multiTouch = false

        initialDistance = 0f
        lastScale = currentScale

        if (!canDragX) {
            next.x = centerX
        }

        if (!canDragY) {
            next.y = centerY
        }

        boundCoordinates()

        if (!canDragX && !canDragY) {

            if (image.isLandscape) {
                currentScale = fitScaleHorizontal
                lastScale = fitScaleHorizontal
            } else {
                currentScale = fitScaleVertical
                lastScale = fitScaleVertical
            }
        }

        image.scale = currentScale
        image.setPosition(next.x, next.y)

        if (imageListener != null) {
            imageListener.onScale(currentScale)
            imageListener.onPosition(next.x, next.y)
        }

        image.redraw()
    }

    protected fun handleScale(scale: Float, x: Float, y: Float) {

        currentScale = scale

        if (currentScale > maxScale) {
            currentScale = maxScale
        } else if (currentScale < minScale) {
            currentScale = minScale
        } else {
            next.x = x
            next.y = y
        }

        calculateBoundaries()

        image.scale = currentScale
        image.setPosition(next.x, next.y)

        if (imageListener != null) {
            imageListener.onScale(currentScale)
            imageListener.onPosition(next.x, next.y)
        }

        image.redraw()
    }

    protected fun handleDrag(x: Float, y: Float): Boolean {
        current.x = x
        current.y = y

        val diffX = current.x - last.x
        val diffY = current.y - last.y

        if (diffX != 0f || diffY != 0f) {

            if (canDragX) next.x += diffX
            if (canDragY) next.y += diffY

            boundCoordinates()

            last.x = current.x
            last.y = current.y

            if (canDragX || canDragY) {
                image.setPosition(next.x, next.y)

                imageListener?.onPosition(next.x, next.y)

                return true
            }
        }

        return false
    }

    fun reset() {
        currentScale = startingScale
        next.x = centerX
        next.y = centerY
        calculateBoundaries()
        image.scale = currentScale
        image.setPosition(next.x, next.y)
        image.redraw()
    }

//    fun setOnClickListener(onClickListener: OnClickListener) {
//        this.onClickListener = onClickListener
//    }
//
//    fun setCanvasWidth(canvasWidth: Int) {
//        this.canvasWidth = canvasWidth
//    }
//
//    fun setCanvasHeight(canvasHeight: Int) {
//        this.canvasHeight = canvasHeight
//    }
//
//    fun setFitScaleHorizontal(fitScale: Float) {
//        this.fitScaleHorizontal = fitScale
//    }
//
//    fun setFitScaleVertical(fitScaleVertical: Float) {
//        this.fitScaleVertical = fitScaleVertical
//    }

    protected fun boundCoordinates() {
        if (next.x < boundaryLeft) {
            next.x = boundaryLeft
        } else if (next.x > boundaryRight) {
            next.x = boundaryRight
        }

        if (next.y < boundaryTop) {
            next.y = boundaryTop
        } else if (next.y > boundaryBottom) {
            next.y = boundaryBottom
        }
    }

    protected fun calculateBoundaries() {

        val effectiveWidth = Math.round(imageWidth.toFloat() * currentScale)
        val effectiveHeight = Math.round(imageHeight.toFloat() * currentScale)

        canDragX = effectiveWidth > displayWidth
        canDragY = effectiveHeight > displayHeight

        if (canDragX) {
            val diff = (effectiveWidth - displayWidth).toFloat() / 2.0f
            boundaryLeft = centerX - diff
            boundaryRight = centerX + diff
        }

        if (canDragY) {
            val diff = (effectiveHeight - displayHeight).toFloat() / 2.0f
            boundaryTop = centerY - diff
            boundaryBottom = centerY + diff
        }
    }
}
