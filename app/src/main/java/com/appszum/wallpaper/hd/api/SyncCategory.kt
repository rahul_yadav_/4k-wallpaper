package com.appszum.wallpaper.hd.api

import android.content.Context
import android.util.Log
import com.appszum.lib.api.OnSuccessListener
import com.appszum.lib.api.Sync
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperDatabase
import com.google.gson.Gson
import org.json.JSONObject
import java.util.*

class SyncCategory(context: Context, listener: OnSuccessListener) : Sync<WallpaperDatabase, Category>(context, listener) {
    override val db: WallpaperDatabase? = WallpaperDatabase.getInstance(context)

    override fun sync() {
        Api.CategoryList.sync(settings.lastSync, listener = this).call(context = context)
    }

    override fun onResponse(response: JSONObject) {
        Log.d("response", response.toString())
        val r: CategoryResponse = Gson().fromJson(response.toString(), CategoryResponse::class.java)
        val result = r.response
        if (result != null) {
            insert(result.records) {
                listener.success()
                settings.lastSync = Date()
            }
            if (result.records.count() < 0) {
                return
            }
        }

    }

    override fun insert(list: ArrayList<Category>, action: (Boolean) -> Unit) {
        val categoryDao = db?.categoryDao()
        if (categoryDao == null) {
            action(false)
            return
        }
        task {
            list.forEach { category: Category ->
                category.update(categoryDao)
            }
            action(true)
        }
    }

}