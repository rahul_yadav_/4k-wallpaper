package com.appszum.wallpaper.hd.task

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.appszum.lib.task.DbWorkerThread
import com.appszum.lib.task.ImageLoadListener
import com.appszum.lib.task.TaskHandler
import com.appszum.lib.utils.ImageCache
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.ImageInfo
import com.appszum.wallpaper.hd.module.WallpaperDatabase

class CategoryImageLoad(private val db: WallpaperDatabase, private val imageCache: ImageCache? = null) : ImageLoadListener<Category>, TaskHandler {
    private val mDbWorkerThread = DbWorkerThread("CategoryImageLoad")
    private val imageInfoDao = db.imageInfoDao()
    private val categoryDao = db.categoryDao()
    override fun save(item: Category, bitmap: Bitmap, saved: ((Boolean) -> Unit)?) {
        task {
            imageCache?.putBitmap(item.key, bitmap)
            item.imageId = imageInfoDao.insert(ImageInfo.get(item, bitmap))
            item.update(categoryDao)
            print("Image Saved for ${item.name} - ${item.dowloaded}")
            ui {
                if (saved != null) {
                    saved(item.imageId != null)
                }
            }
        }
    }

    override fun load(item: Category, load: (Bitmap?) -> Unit) {
        task {
            var bitmap: Bitmap? = imageCache?.getBitmap(item.key)
            if (bitmap == null) {
                val imageId = item.imageId
                val imageInfo = if (imageId != null) imageInfoDao.info(imageId) else null
                var image: ByteArray? = null
                if (imageInfo != null) {
                    if (imageInfo.isSameUrl(item)) {
                        image = imageInfo.image
                    } else {
                        item.imageId = null
                        if (imageId != null) {
                            imageInfoDao.remove(imageId)
                        }
                        item.update(categoryDao)
                        print("Image removed for ${item.name} - ${item.dowloaded}")
                    }
                }

                bitmap = if (image != null) BitmapFactory.decodeByteArray(image, 0, image.size) else null
            }
            ui {
                load(bitmap)
            }
        }
    }

    override fun task(action: () -> Unit) {
        mDbWorkerThread.task(action)
    }

    override fun ui(action: () -> Unit) {
        mDbWorkerThread.ui(action)
    }
}