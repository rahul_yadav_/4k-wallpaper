package com.appszum.wallpaper.hd.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.Window
import com.appszum.lib.load
import com.appszum.lib.task.DownloadListener
import com.appszum.lib.utils.Utility
import com.appszum.lib.view.ad.FacebookAdView
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.appszum.wallpaper.hd.task.BitmapDownloadTask
import com.facebook.ads.NativeAd
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dowload_dialog.*


/**
 * Created by Indpro on 12/26/2014.
 */
class DownloadDialog private constructor(context: Context, val wallpaperInfo: WallpaperInfo, val listener: Listener, result: Bitmap? = null) : Dialog(context), DownloadListener<Bitmap> {

    private var task: BitmapDownloadTask? = null


    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dowload_dialog)
        show()
        tvSize.text = wallpaperInfo.sizeInfo
        tvDownload.text = Utility.count(wallpaperInfo.download)
        tvLike.text = Utility.count(wallpaperInfo.likes)
        tvView.text = Utility.count(wallpaperInfo.views)

        @SuppressLint("SetTextI18n")
        tvInfo.text = "${wallpaperInfo.width}x${wallpaperInfo.height}"

        btnInfo.setOnClickListener {
            dismiss()
            if (result != null) {
                listener.info(wallpaperInfo)
            }
        }
        btnSetWallpaper.setOnClickListener {
            setWallpaper(it)
        }
    }


    private var result: Bitmap? = null
        set(value) {
            field = value
            if (value == null) {
                return
            }
            btnInfo.isEnabled = true
            btnSetWallpaper.isEnabled = true
            download_text.text = context.getString(R.string.successfully_downloaded)
            download_text.setTextColor(Color.GREEN)
            progress_bar.foregroundProgressColor = Color.GREEN

            setCancelable(true)
            btnSetWallpaper.isEnabled = true
            btnInfo.isEnabled = true

            btnInfo.text = context.getString(R.string.info)
            btnSetWallpaper.text = context.getString(R.string.set_wallpaper)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        download()
        Picasso.get().load(wallpaperInfo.thumbnails).into(ivIcon)

        fbNativeAdLayout.visibility = View.GONE

        NativeAd(context, "757735714253412_2430962503597383").load {
            fbNativeAdLayout.visibility = View.VISIBLE
            FacebookAdView<Any>(fbNativeAdLayout, it).inflateAd(it, fbNativeAdLayout)
        }
    }

    private val isDownloading: Boolean get() = task != null


    fun download() {
        if (result != null) {
            return
        }
        setCancelable(false)
        if (!isDownloading) {
            task = BitmapDownloadTask(wallpaperInfo, this)
            task?.execute()
        }
    }

    private fun setWallpaper(view: View) {
        if (this.result == null) {
            view.isEnabled = false
            download()
            return
        }
        dismiss()
        listener.setWallpaper(wallpaperInfo)
    }


    @SuppressLint("SetTextI18n")
    override fun onProgressUpdate(progress: Float) {
        task = null
        download_text.text = "${String.format("%.2f", progress)}% downloading..."
        progress_bar.progress = progress
    }

    override fun success(result: Bitmap) {
        download_text.text = context.getString(R.string.saving_on_db)
        progress_bar.progress = 100F
        listener.save(wallpaperInfo, result) {
            if (!it) {
                error()
                return@save
            }
            this.result = result
            task = null
        }
    }

    override fun dismiss() {
        task?.cancel(true)
        super.dismiss()
    }

    override fun error() {
        task = null
        setCancelable(true)
        download_text.text = context.getString(R.string.problem_with_image_downloading)
        btnSetWallpaper.text = context.getString(R.string.try_again)
        btnSetWallpaper.isEnabled = true
    }

    companion object {

        fun show(context: Context, wallpaperInfo: WallpaperInfo, listener: Listener): DownloadDialog {
            return DownloadDialog(context, wallpaperInfo, listener)
        }
    }

    interface Listener {
        fun save(wallpaperInfo: WallpaperInfo, bitmap: Bitmap, onSave: (Boolean) -> Unit)

        fun info(wallpaperInfo: WallpaperInfo)

        fun setWallpaper(wallpaperInfo: WallpaperInfo)
    }
}