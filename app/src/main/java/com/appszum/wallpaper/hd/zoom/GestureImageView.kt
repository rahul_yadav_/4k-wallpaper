/*
 * Copyright (c) 2012 Jason Polites
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appszum.wallpaper.hd.zoom

import android.content.Context
import android.content.res.Configuration
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import android.support.annotation.DrawableRes
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup.LayoutParams
import android.widget.ImageView
import java.io.InputStream
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit

class GestureImageView : android.support.v7.widget.AppCompatImageView {

    private val drawLock = Semaphore(0)
    private var animator: Animator? = null
    private var drawable: Drawable? = null

    var imageX = 0f
        private set
    var imageY = 0f
        private set

    private var layout = false

    private var scaleAdjust = 1.0f
    private var defaultImageViewListener: GestureImageViewListener = object : GestureImageViewListener {

        override fun onTouch(x: Float, y: Float) {
            // TODO Auto-generated method stub
        }

        override fun onScale(scale: Float) {
            // TODO Auto-generated method stub
        }

        override fun onPosition(x: Float, y: Float) {
            val halfImage = this@GestureImageView.scaledWidth / 2.0
            val viewWidth = this@GestureImageView.width.toDouble()
            println(x)
            println(halfImage.toString() + "::" + (viewWidth - halfImage))
            this@GestureImageView.parent.requestDisallowInterceptTouchEvent(!(x.toDouble() == halfImage || x.toDouble() == viewWidth - halfImage))
        }
    }
    private var startingScale = -1.0f
    var scale: Float
        get() = scaleAdjust
        set(scale) {
            scaleAdjust = scale
            print(scaleAdjust)
        }

    private var maxScale = 5.0f
        set(value) {
            field = value
            gestureImageViewTouchListener?.maxScale = value * startingScale
        }
    var minScale: Float = 0.50f
        set(value) {
            field = value
            gestureImageViewTouchListener?.minScale = value * fitScaleHorizontal
        }
    private var fitScaleHorizontal = 1.0f
    private var fitScaleVertical = 1.0f
    private var _rotation = 0.0f
    var centerX: Float = 0.toFloat()
        private set
    var centerY: Float = 0.toFloat()
        private set
    private var startX: Float? = null
    private var startY: Float? = null
    private var hWidth: Int = 0
    private var hHeight: Int = 0
    private var resId = -1
    var isRecycle = false
    var isStrict = false
    private var displayHeight: Int = 0
    private var displayWidth: Int = 0
    private var alpha = 255
    private var _colorFilter: ColorFilter? = null
    var deviceOrientation = -1
        private set
    private var imageOrientation: Int = 0
    var gestureImageViewListener: GestureImageViewListener? = null
        get() = field ?: defaultImageViewListener

    private var gestureImageViewTouchListener: GestureImageViewTouchListener? = null
        set(listener) {

            if (listener != null) {
                listener.minScale = minScale * if (isLandscape) fitScaleHorizontal else fitScaleVertical

                listener.maxScale = maxScale * startingScale

                listener.fitScaleHorizontal = fitScaleHorizontal
                listener.fitScaleVertical = fitScaleVertical
                listener.canvasWidth = measuredWidth
                listener.canvasHeight = measuredHeight
                listener.onClickListener = clickListener

            }
            field = listener
        }
    private var customOnTouchListener: View.OnTouchListener? = null
    private var clickListener: View.OnClickListener? = null


    protected val isRecycled: Boolean
        get() {
            return (drawable as? BitmapDrawable)?.bitmap?.isRecycled ?: false
        }

    val scaledWidth: Int
        get() = Math.round(imageWidth * scale)

    val scaledHeight: Int
        get() = Math.round(imageHeight * scale)

    val imageWidth: Int
        get() = drawable?.intrinsicWidth ?: 0

    val imageHeight: Int
        get() = drawable?.intrinsicHeight ?: 0

    val isLandscape: Boolean
        get() = imageWidth >= imageHeight

    val isPortrait: Boolean
        get() = imageWidth <= imageHeight

    /**
     * Returns true if the image dimensions are aligned with the orientation of the device.
     *
     * @return
     */
    val isOrientationAligned: Boolean
        get() {
            if (deviceOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                return isLandscape
            } else if (deviceOrientation == Configuration.ORIENTATION_PORTRAIT) {
                return isPortrait
            }
            return true
        }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : this(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        val scaleType = attrs.getAttributeValue(GLOBAL_NS, "scaleType")

        if (scaleType == null || scaleType.trim { it <= ' ' }.isEmpty()) {
            setScaleType(ImageView.ScaleType.CENTER_INSIDE)
        }

        val strStartX = attrs.getAttributeValue(LOCAL_NS, "start-x")
        val strStartY = attrs.getAttributeValue(LOCAL_NS, "start-y")

        if (strStartX != null && strStartX.trim { it <= ' ' }.length > 0) {
            startX = java.lang.Float.parseFloat(strStartX)
        }

        if (strStartY != null && strStartY.trim { it <= ' ' }.length > 0) {
            startY = java.lang.Float.parseFloat(strStartY)
        }

        startingScale = attrs.getAttributeFloatValue(LOCAL_NS, "start-scale", startingScale)
        minScale = attrs.getAttributeFloatValue(LOCAL_NS, "min-scale", minScale)
        maxScale = attrs.getAttributeFloatValue(LOCAL_NS, "max-scale", maxScale)
        isStrict = attrs.getAttributeBooleanValue(LOCAL_NS, "strict", isStrict)
        isRecycle = attrs.getAttributeBooleanValue(LOCAL_NS, "recycle", isRecycle)
        initImage()
    }

    constructor(context: Context) : super(context) {
        scaleType = ImageView.ScaleType.CENTER_INSIDE
        initImage()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        if (drawable != null) {
            val orientation = resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                displayHeight = View.MeasureSpec.getSize(heightMeasureSpec)

                if (layoutParams.width == LayoutParams.WRAP_CONTENT) {
                    val ratio = imageWidth.toFloat() / imageHeight.toFloat()
                    displayWidth = Math.round(displayHeight.toFloat() * ratio)
                } else {
                    displayWidth = View.MeasureSpec.getSize(widthMeasureSpec)
                }
            } else {
                displayWidth = View.MeasureSpec.getSize(widthMeasureSpec)
                if (layoutParams.height == LayoutParams.WRAP_CONTENT) {
                    val ratio = imageHeight.toFloat() / imageWidth.toFloat()
                    displayHeight = Math.round(displayWidth.toFloat() * ratio)
                } else {
                    displayHeight = View.MeasureSpec.getSize(heightMeasureSpec)
                }
            }
        } else {
            displayHeight = View.MeasureSpec.getSize(heightMeasureSpec)
            displayWidth = View.MeasureSpec.getSize(widthMeasureSpec)
        }

        setMeasuredDimension(displayWidth, displayHeight)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (changed || !layout) {
            setupCanvas(displayWidth, displayHeight, resources.configuration.orientation)
        }
    }

    protected fun setupCanvas(measuredWidth: Int, measuredHeight: Int, orientation: Int) {
        var _measuredWidth = measuredWidth
        var _measuredHeight = measuredHeight

        if (deviceOrientation != orientation) {
            layout = false
            deviceOrientation = orientation
        }

        if (drawable != null && !layout) {
            val imageWidth = imageWidth
            val imageHeight = imageHeight

            hWidth = Math.round(imageWidth.toFloat() / 2.0f)
            hHeight = Math.round(imageHeight.toFloat() / 2.0f)

            _measuredWidth -= paddingLeft + paddingRight
            _measuredHeight -= paddingTop + paddingBottom

            computeCropScale(imageWidth, imageHeight, _measuredWidth, _measuredHeight)

            if (startingScale <= 0.0f) {
                computeStartingScale(imageWidth, imageHeight, _measuredWidth, _measuredHeight)
            }

            scaleAdjust = startingScale

            this.centerX = _measuredWidth.toFloat() / 2.0f
            this.centerY = _measuredHeight.toFloat() / 2.0f

            imageX = startX ?: centerX
            imageY = startY ?: centerY

            gestureImageViewTouchListener = GestureImageViewTouchListener(this, _measuredWidth, _measuredHeight)



            drawable?.setBounds(-hWidth, -hHeight, hWidth, hHeight)

            super.setOnTouchListener { v, event ->
                customOnTouchListener?.onTouch(v, event)
                gestureImageViewTouchListener?.onTouch(v, event) ?: true
            }

            layout = true
        }
    }

    protected fun computeCropScale(imageWidth: Int, imageHeight: Int, measuredWidth: Int, measuredHeight: Int) {
        fitScaleHorizontal = measuredWidth.toFloat() / imageWidth.toFloat()
        fitScaleVertical = measuredHeight.toFloat() / imageHeight.toFloat()
    }

    protected fun computeStartingScale(imageWidth: Int, imageHeight: Int, measuredWidth: Int, measuredHeight: Int) {
        when (scaleType) {
            ImageView.ScaleType.CENTER ->
                // Center the image in the view, but perform no scaling.
                startingScale = 1.0f

            ImageView.ScaleType.CENTER_CROP ->
                // Scale the image uniformly (maintain the image's aspect ratio) so that both dimensions
                // (width and height) of the image will be equal to or larger than the corresponding dimension of the view (minus padding).
                startingScale = Math.max(measuredHeight.toFloat() / imageHeight.toFloat(), measuredWidth.toFloat() / imageWidth.toFloat())

            ImageView.ScaleType.CENTER_INSIDE -> {

                // Scale the image uniformly (maintain the image's aspect ratio) so that both dimensions
                // (width and height) of the image will be equal to or less than the corresponding dimension of the view (minus padding).
                val wRatio = imageWidth.toFloat() / measuredWidth.toFloat()
                val hRatio = imageHeight.toFloat() / measuredHeight.toFloat()

                startingScale = if (wRatio < hRatio) fitScaleHorizontal else fitScaleVertical
            }
        }
    }

    protected fun recycle() {
        if (isRecycle && drawable is BitmapDrawable) {
            (drawable as BitmapDrawable).bitmap?.recycle()
        }
    }

    override fun onDraw(canvas: Canvas) {
        if (layout) {
            if (drawable != null && !isRecycled) {
                canvas.save()

                val adjustedScale = scale * scaleAdjust

                canvas.translate(imageX, imageY)

                if (_rotation != 0.0f) {
                    canvas.rotate(_rotation)
                }

                if (adjustedScale != 1.0f) {
                    canvas.scale(adjustedScale, adjustedScale)
                }

                drawable?.draw(canvas)

                canvas.restore()
            }

            if (drawLock.availablePermits() <= 0) {
                drawLock.release()
            }
        }
    }

    /**
     * Waits for a draw
     *
     * @param max time to wait for draw (ms)
     * @throws InterruptedException
     */
    @Throws(InterruptedException::class)
    fun waitForDraw(timeout: Long): Boolean {
        return drawLock.tryAcquire(timeout, TimeUnit.MILLISECONDS)
    }

    override fun onAttachedToWindow() {
        animator = Animator(this, "GestureImageViewAnimator")
        animator?.start()

        if (resId >= 0 && drawable == null) {
            setImageResource(resId)
        }

        super.onAttachedToWindow()
    }

    fun animationStart(animation: Animation) {
        animator?.play(animation)
    }

    fun animationStop() {
        animator?.cancel()
    }

    override fun onDetachedFromWindow() {
        animator?.finish()
        if (isRecycle && drawable != null && !isRecycled) {
            recycle()
            drawable = null
        }
        super.onDetachedFromWindow()
    }

    protected fun initImage() {
        this.drawable?.alpha = alpha
        this.drawable?.isFilterBitmap = true
        this.drawable?.colorFilter = _colorFilter

        if (!layout) {
            requestLayout()
            redraw()
        }
    }

    override fun setImageBitmap(image: Bitmap) {
        this.drawable = BitmapDrawable(resources, image)
        initImage()
    }

    override fun setImageDrawable(drawable: Drawable?) {
        this.drawable = drawable
        initImage()
    }

    override fun setImageResource(
            @DrawableRes id: Int) {
        if (drawable != null) {
            this.recycle()
        }
        this.resId = id
        setImageDrawable(context.getDrawable(id))
    }

    fun setStartingPosition(x: Float, y: Float) {
        this.startX = x
        this.startY = y
    }

    fun moveBy(x: Float, y: Float) {
        this.imageX += x
        this.imageY += y
    }

    fun setPosition(x: Float, y: Float) {
        this.imageX = x
        this.imageY = y
    }

    fun redraw() {
        postInvalidate()
    }


    fun reset() {
        imageX = centerX
        imageY = centerY
        scaleAdjust = startingScale
        gestureImageViewTouchListener?.reset()
        redraw()
    }

    override fun setRotation(rotation: Float) {
        this._rotation = rotation
    }

    override fun getDrawable(): Drawable? {
        return drawable
    }

    override fun setAlpha(alpha: Int) {
        this.alpha = alpha
        drawable?.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter) {
        this._colorFilter = cf
        drawable?.colorFilter = cf
    }

    override fun setImageURI(mUri: Uri?) {

        if (mUri != null && "content" == mUri.scheme) {
            try {
                val orientationColumn = arrayOf(MediaStore.Images.Media.ORIENTATION)

                val cur = context.contentResolver.query(mUri, orientationColumn, null, null, null)

                if (cur != null && cur.moveToFirst()) {
                    imageOrientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]))
                }

                var `in`: InputStream? = null

                try {
                    `in` = context.contentResolver.openInputStream(mUri)
                    val bmp = BitmapFactory.decodeStream(`in`)

                    if (imageOrientation != 0) {
                        val m = Matrix()
                        m.postRotate(imageOrientation.toFloat())
                        val rotated = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, m, true)
                        bmp.recycle()
                        setImageDrawable(BitmapDrawable(resources, rotated))
                    } else {
                        setImageDrawable(BitmapDrawable(resources, bmp))
                    }
                } finally {
                    `in`?.close()

                    cur?.close()
                }
            } catch (e: Exception) {
                Log.w("GestureImageView", "Unable to open content: $mUri", e)
            }

        } else if (mUri != null) {
            setImageDrawable(Drawable.createFromPath(mUri.toString()))
        }

        if (drawable == null) {
            Log.e("GestureImageView", "resolveUri failed on bad bitmap uri: $mUri")
        }
    }

    override fun getImageMatrix(): Matrix {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
        return super.getImageMatrix()
    }

    override fun setImageMatrix(matrix: Matrix) {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
    }

    override fun setScaleType(scaleType: ImageView.ScaleType) {
        if (scaleType == ImageView.ScaleType.CENTER ||
                scaleType == ImageView.ScaleType.CENTER_CROP ||
                scaleType == ImageView.ScaleType.CENTER_INSIDE) {

            super.setScaleType(scaleType)
        } else if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
    }

    override fun invalidateDrawable(dr: Drawable) {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
        super.invalidateDrawable(dr)
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
        return super.onCreateDrawableState(extraSpace)
    }

    override fun setAdjustViewBounds(adjustViewBounds: Boolean) {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
        super.setAdjustViewBounds(adjustViewBounds)
    }

    override fun setImageLevel(level: Int) {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
        super.setImageLevel(level)
    }

    override fun setImageState(state: IntArray, merge: Boolean) {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
    }

    override fun setSelected(selected: Boolean) {
        if (isStrict) {
            throw UnsupportedOperationException("Not supported")
        }
        super.setSelected(selected)
    }

    override fun setOnTouchListener(l: View.OnTouchListener) {
        this.customOnTouchListener = l
    }


    override fun setOnClickListener(clickListener: View.OnClickListener?) {
        this.clickListener = clickListener
        gestureImageViewTouchListener?.onClickListener = clickListener
    }

    companion object {

        val GLOBAL_NS = "http://schemas.android.com/apk/res/android"
        val LOCAL_NS = "http://schemas.polites.com/android"
    }
}
