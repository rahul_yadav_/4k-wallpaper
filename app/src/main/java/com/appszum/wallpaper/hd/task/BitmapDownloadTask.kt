package com.appszum.wallpaper.hd.task

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import com.appszum.lib.task.DownloadListener

import com.appszum.lib.toByteArray
import com.appszum.wallpaper.hd.BuildConfig
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.module.WallpaperInfo
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class BitmapDownloadTask(val wallpaperInfo: WallpaperInfo, val listener: DownloadListener<Bitmap>) : AsyncTask<String, Float, Bitmap?>() {

    override fun onProgressUpdate(vararg values: Float?) {
        super.onProgressUpdate(*values)
        if (values.isEmpty()) {
            return
        }

        val progress: Float = values[0] ?: 0f
        listener.onProgressUpdate(if (progress >= 100) 99.99f else progress)
    }

    override fun onPostExecute(result: Bitmap?) {
        super.onPostExecute(result)
        if (result == null) {
            listener.error()
            return
        }
        result.toByteArray()
        listener.success(result)
    }

    override fun doInBackground(vararg params: String?): Bitmap? {
        var input: InputStream? = null
        var output: ByteArrayOutputStream? = null
        var connection: HttpURLConnection? = null
        try {
            val url = URL("${Api.HOST}/api/wallpaper/category/download")
            connection = url.openConnection() as HttpURLConnection
            connection.setRequestProperty("key", BuildConfig.API_KEY)
            connection.requestMethod = "POST"
            connection.doInput = true

            val array = JSONArray()
            val item = JSONObject()
            val parent = JSONObject()
            item.put("key", wallpaperInfo.key)

            item.put("catkey", wallpaperInfo.catkey)
            array.put(item)
            parent.put("items", array)



            connection.connect()

            val wr = OutputStreamWriter(connection.outputStream)
            wr.write(parent.toString())
            wr.flush()

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                return null
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            val fileLength = connection.contentLength.toFloat()

            // download the file
            input = connection.inputStream

            val data = ByteArray(wallpaperInfo.size.toInt())
            output = ByteArrayOutputStream()


            var len = input.read(data)
            var total = len.toLong()

            while (len != -1) {
                // allow canceling with back button
                if (isCancelled) {
                    input.close()
                    return null
                }
                total += len.toLong()

                if (fileLength > 0) {
                    publishProgress((total * 100) / fileLength)
                }


                output.write(data, 0, len)
                len = input.read(data)
            }
            //return BitmapFactory.decodeStream(BufferedInputStream(input));
            val image = output.toByteArray()
            return BitmapFactory.decodeByteArray(image, 0, image.size)
        } catch (e: Exception) {
            throw e
        } finally {
            try {
                input?.close()
                output?.close()

            } catch (ignored: IOException) {
            }
            connection?.disconnect()

        }
    }
}
