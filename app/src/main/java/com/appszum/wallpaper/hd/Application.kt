package com.appszum.wallpaper.hd

import com.appszum.wallpaper.hd.module.WallpaperDatabase
import com.facebook.ads.AudienceNetworkAds
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterConfig


class Application : com.appszum.lib.Application() {

    override fun onCreate() {
        super.onCreate()
        val authConfig = TwitterAuthConfig("plWuvRy4allFj1JKtfaVlVMKR", "ZIPgp63wUQnQZJIR4HrSBILwjzk2cE0DGMXITZ67oXXLnPEvDM")

        val twitterConfig = TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();

        Twitter.initialize(twitterConfig);
        WallpaperDatabase.getInstance(this)
        MobileAds.initialize(this, getString(R.string.add_mob));
        AudienceNetworkAds.initialize(this);
        if (BuildConfig.DEBUG) {
            AdRequest.Builder().addTestDevice("548FDBA6FBE7080296BF09D5EA9FF51E")
        }
    }
}