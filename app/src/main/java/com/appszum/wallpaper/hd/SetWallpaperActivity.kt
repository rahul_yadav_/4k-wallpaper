package com.appszum.wallpaper.hd

import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.android.volley.VolleyError
import com.appszum.lib.BaseActivity
import com.appszum.lib.blur
import com.appszum.lib.task.DbWorkerThread
import com.appszum.lib.task.TaskHandler
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.module.WallpaperDatabase
import com.appszum.wallpaper.hd.module.WallpaperInfo
import kotlinx.android.synthetic.main.activity_set_wallpaper.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class SetWallpaperActivity : BaseActivity(), TaskHandler {
    private val mHideHandler = Handler()
    private val mHidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
//        fullscreen_content.systemUiVisibility =
//                View.SYSTEM_UI_FLAG_LOW_PROFILE or
//                View.SYSTEM_UI_FLAG_FULLSCREEN or
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
//                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
//                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
//                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }
    private val mShowPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
        fullscreen_content_controls.visibility = View.VISIBLE
    }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private val mDelayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    private lateinit var mDbWorkerThread: DbWorkerThread
    private lateinit var db: WallpaperDatabase
    private lateinit var wallpaperInfo: WallpaperInfo
    private var which: Which = Which.BOTH
        set(value) {
            field = value
            if (value == Which.BOTH) {
                lSingle.visibility = View.GONE
                lBoth.visibility = View.VISIBLE
            } else {
                lSingle.visibility = View.VISIBLE
                lBoth.visibility = View.GONE
            }
        }

    private var bitmap: Bitmap? = null
        set(bitmap) {
            field = bitmap
            if (bitmap != null) {
                frame.background = BitmapDrawable(bitmap.blur(this))
                ivBoth.background = BitmapDrawable(bitmap)
                ivLock.background = BitmapDrawable(bitmap)
                ivHome.background = BitmapDrawable(bitmap)
                btnSetWallpaer.isEnabled = true
                btnSetWallpaer.visibility = View.VISIBLE
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        db = WallpaperDatabase.getInstance(this)!!
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_set_wallpaper)
        wallpaperInfo = WallpaperInfo.get(intent)!!
        which = Which.get(intent)!!
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mVisible = true

        load(wallpaperInfo) {
            task {
                if (it != null) {
                    this.bitmap = it
                    return@task
                }
                showProgressAlert("fetching image...")
                Api.ImageRequest.image(wallpaperInfo).call(this, object : com.appszum.lib.api.Api.VolleyListener<Bitmap> {
                    override fun onResponse(response: Bitmap) {
                        this@SetWallpaperActivity.bitmap = response
                        dismissProgressAlert()

                    }

                    override fun onErrorResponse(error: VolleyError) {
                        dismissProgressAlert()
                    }
                })
            }

        }

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        fullscreen_content_controls.visibility = View.GONE
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {
        // Show the system bar
//        //fullscreen_content.systemUiVisibility =
//                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
//                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300


        fun intent(context: Context, wallpaperInfo: WallpaperInfo, which: Which): Intent {
            val intent = Intent(context, SetWallpaperActivity::class.java)
            return wallpaperInfo.intent(which.intent(intent))
        }
    }

    fun load(item: WallpaperInfo, load: (Bitmap?) -> Unit) {
        task {
            val imageInfo = db.imageInfoDao().info(item.key, 2)
            ui {
                load(imageInfo?.bitmap)
            }
        }
    }

    override fun task(action: () -> Unit) {
        mDbWorkerThread.task(action)
    }

    override fun ui(action: () -> Unit) {
        mDbWorkerThread.ui(action)
    }


    enum class Which {
        HOME, LOCK, BOTH;

        companion object {
            fun get(intent: Intent): Which? {
                return if (intent.hasExtra("Which")) intent.getSerializableExtra("Which") as Which else null
            }
        }

//        val which: Int get(){
//            if(this == HOME){
//                return WallpaperManager.FLAG_SYSTEM
//            }
//
//            if(this == LOCK){
//
//            }
//            return WallpaperManager.FLAG_SYSTEM  WallpaperManager.FL
//        }

        fun apply(manager: WallpaperManager) {
            //if ()
        }

        val isHome: Boolean get() = this == HOME || this == BOTH

        val isLock: Boolean get() = this == LOCK || this == BOTH

        fun intent(intent: Intent): Intent {
            intent.putExtra("Which", this)
            return intent
        }
    }
}
