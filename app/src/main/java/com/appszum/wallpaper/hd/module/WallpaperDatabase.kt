package com.appszum.wallpaper.hd.module

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [Category::class, WallpaperInfo::class, ImageInfo::class], version = 1, exportSchema = false)
abstract class WallpaperDatabase : RoomDatabase() {

    abstract fun categoryDao(): CategoryDao
    abstract fun wallpaperInfoDao(): WallpaperInfoDao
    abstract fun imageInfoDao(): ImageInfoDao

    companion object {
        private var INSTANCE: WallpaperDatabase? = null

        fun getInstance(context: Context): WallpaperDatabase? {
            if (INSTANCE == null) {
                synchronized(WallpaperDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            WallpaperDatabase::class.java, "WallpaperDatabase.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}