package com.appszum.wallpaper.hd.adapter


import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.appszum.lib.adapter.RecyclerViewAdapter
import com.appszum.lib.listener.RecyclerViewListener
import com.appszum.lib.task.ImageLoadListener
import com.appszum.lib.view.RecycleViewHolder
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso


class WallpaperAdapter(context: Context, @LayoutRes val itemLayout: Int = R.layout.wallpaper_item1, listener: RecyclerViewListener<WallpaperInfo>, val imageLoadListener: ImageLoadListener<WallpaperInfo>? = null) : RecyclerViewAdapter<RecycleViewHolder<WallpaperInfo>, WallpaperInfo>(listener) {

    interface AttachListener {
        var listener: RecyclerViewListener<WallpaperInfo>
    }

    init {
        // adListener = AdMobAdListener(context) { notifyDataChanged() }
    }


    override fun getItemView(parent: ViewGroup): RecycleViewHolder<WallpaperInfo> = WallpaperViewHolder.get(parent, itemLayout, listener, imageLoadListener)

    class WallpaperViewHolder(itemView: View, recyclerListener: RecyclerViewListener<WallpaperInfo>?, var imageLoadListener: ImageLoadListener<WallpaperInfo>?) : RecycleViewHolder<WallpaperInfo>(itemView, recyclerListener) {

        companion object {
            fun get(parent: ViewGroup, @LayoutRes itemLayout: Int, recyclerListener: RecyclerViewListener<WallpaperInfo>?, imageLoadListener: ImageLoadListener<WallpaperInfo>?): WallpaperViewHolder {
                return WallpaperViewHolder(LayoutInflater.from(parent.context).inflate(itemLayout, parent, false), recyclerListener, imageLoadListener)
            }
        }

        private val icon: ImageView = findViewById(R.id.icon)

        init {
            icon.scaleType = ImageView.ScaleType.FIT_CENTER
        }

        override fun setItem(value: WallpaperInfo?) {
            super.setItem(value)

            if (value != null) {
                if (imageLoadListener == null) {
                    loadImage(value)
                    return
                }

                imageLoadListener?.load(value) {
                    if (it != null) {
                        icon.scaleType = ImageView.ScaleType.FIT_XY
                        icon.setImageBitmap(it)
                        return@load
                    }
                    loadImage(value)
                    return@load
                }
            }
        }

        private fun loadImage(wallpaperInfo: WallpaperInfo) {
            val icon = this.icon
            icon.scaleType = ImageView.ScaleType.FIT_CENTER
            Picasso.get()
                    .load(wallpaperInfo.thumbnails)
                    .placeholder(R.drawable.loading_text)
                    .into(icon, object : Callback {
                        override fun onSuccess() {
                            icon.scaleType = ImageView.ScaleType.FIT_XY
                        }

                        override fun onError(e: java.lang.Exception?) {

                        }
                    })
        }
    }

}
