package com.appszum.wallpaper.hd

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.view.MenuItem
import android.view.View
import com.appszum.lib.NavigationTabActivity
import com.appszum.lib.api.OnSuccessListener
import com.appszum.lib.dialog.AlertView
import com.appszum.lib.listener.RecyclerListener
import com.appszum.lib.listener.RecyclerViewListener
import com.appszum.lib.model.LoginUser
import com.appszum.lib.utils.ImageCache
import com.appszum.lib.utils.Utility
import com.appszum.lib.view.RecycleViewHolder
import com.appszum.wallpaper.hd.adapter.CategoryAdapter
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.SyncCategory
import com.appszum.wallpaper.hd.fragment.CategoryFragment
import com.appszum.wallpaper.hd.module.Category

class NavigationActivity : NavigationTabActivity(), CategoryAdapter.AttachListener {


    private lateinit var syncApi: SyncCategory

    val imageCache = ImageCache()

    override val tabList: MutableList<String> = ArrayList()

    override val navigationView: NavigationView
        get() = findViewById(R.id.nav_view)

    override val drawerLayout: DrawerLayout
        get() = findViewById(R.id.drawer_layout)

    override val layout: Int
        get() = R.layout.activity_navigation


    override var listener: RecyclerViewListener<Category> = object : RecyclerListener<Category>() {
        override fun onItemClick(holder: RecycleViewHolder<Category>, value: Category) {
            super.onItemClick(holder, value)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivity(WallpaperListActivity.intent(this@NavigationActivity, value), ActivityOptions.makeSceneTransitionAnimation(this@NavigationActivity, holder.itemView, getString(R.string.app_home)).toBundle())
            } else {
                startActivity(WallpaperListActivity.intent(this@NavigationActivity, value))
            }
        }

        override fun onItemLongClick(holder: RecycleViewHolder<Category>, value: Category) {
            super.onItemLongClick(holder, value)
            if (user?.admin == true) {

            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        tabList.add("ALL")
        tabList.add("TOP 20")
        super.onCreate(savedInstanceState)
        syncApi = SyncCategory(this, object : OnSuccessListener {
            override fun success() {
                adapter?.fragments?.forEach { fragment ->
                    (fragment as? CategoryFragment)?.loadCategory()
                }
            }

            override fun error() {

            }
        })
        syncApi.sync()
    }

    override fun search(query: String) {
        (adapter?.current as? CategoryFragment)?.filter = query
    }

    override fun newInstance(position: Int): Fragment {
        val fragment = CategoryFragment.newInstance(this, position)
        fragment.imageCache = imageCache
        return fragment
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
//            R.id.nav_camera -> {
//                // Handle the camera action
//            }
            R.id.all -> {
                currentItem = 0
            }
            R.id.top_20 -> {
                currentItem = 1
            }
            R.id.top_download -> {
                startActivity(WallpaperListActivity.intent(this, Api.WallpaperList.TopColumn.download))
            }
            R.id.top_likes -> {
                startActivity(WallpaperListActivity.intent(this, Api.WallpaperList.TopColumn.likes))
            }
            R.id.top_view -> {
                startActivity(WallpaperListActivity.intent(this, Api.WallpaperList.TopColumn.views))
            }
            R.id.favorite -> {
                startActivity(WallpaperListActivity.intent(this, Api.WallpaperList.TopColumn.favorite))
            }
            R.id.downloaded -> {
                startActivity(WallpaperListActivity.intent(this, Api.WallpaperList.TopColumn.downloaded))
            }
            R.id.db_all -> {
                startActivity(WallpaperListActivity.intent(this, Api.WallpaperList.TopColumn.all))
            }
            R.id.nav_share -> {
                Utility.Launch.shareApp(this)
            }

            R.id.about -> {
                startActivity(Intent(this, AboutActivity::class.java))
            }
        }

        closeDrawer()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val adapter: CategoryAdapter? = (adapter?.first as? CategoryFragment)?.categoryAdapter
        val setting = adapter?.setting

        if (item != null) {
            when (item.itemId) {
                R.id.sort_by -> {
                    AlertView.popUpDialog<String>(this, title = getString(R.string.sort_by), values = resources.getStringArray(R.array.sort_by_list), select = setting?.sortBy
                            ?: 0, action = { i: Int, _: String? ->
                        setting?.sortBy = i
                        adapter?.notifyDataChanged()
                    }).show()
                }

                R.id.order_by -> {
                    AlertView.popUpDialog<String>(this, title = getString(R.string.order_by), values = resources.getStringArray(R.array.order_by_list), select = if (setting?.ascending == true) 0 else 1, action = { i: Int, _: String? ->
                        setting?.ascending = i == 0
                        adapter?.notifyDataChanged()
                    }).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun profileInfo(view: View) {
        closeDrawer()
        if (LoginUser.currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
            return
        }
        startActivity(Intent(this, ProfileActivity::class.java))
    }

}