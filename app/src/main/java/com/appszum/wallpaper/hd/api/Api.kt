package com.appszum.wallpaper.hd.api

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.widget.ImageView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.android.volley.toolbox.Volley
import com.appszum.lib.FirebaseRemoteConfig

import com.appszum.lib.syncFormat
import com.appszum.lib.utils.ImageCache
import com.appszum.wallpaper.hd.BuildConfig
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.appszum.wallpaper.hd.module.WallpaperInfoDao
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class Api : com.appszum.lib.api.Api() {

    companion object {
        val HOST: String
            get() {
                val host = FirebaseRemoteConfig.HOST
                return if (host.isEmpty()) BuildConfig.HOST else host
            }
    }


    // GET http://localhost:5001/api/wallpaper

    //http://appszum.com/api/local/wallpaper/category/oubwe5oetax

    //http://localhost:5001/api/wallpaper?top=10
    //http://localhost:5001/api/wallpaper?size=10&offset=20
    //http://localhost:5001/api/wallpaper/category/f5bcquuwbuc?size=10&offset=20

    //pager key (size = 10, offset = 20 , order = "1,2", top = 10)

    //http://localhost:5001/api/wallpaper/category/top/column?top=20
    //top wallpaper column (views, download, likes, dislike)

    class CategoryList(url: String, listener: VolleyListener<JSONObject>) :
            AuthJsonObjectRequest(Method.GET, url, null, listener) {

        companion object {
            fun top(listener: VolleyListener<JSONObject>): CategoryList {
                return CategoryList("${Api.HOST}/api/wallpaper?top=20", listener)
            }

            fun list(offset: Int, size: Int = 20, listener: VolleyListener<JSONObject>): CategoryList {
                return CategoryList("${Api.HOST}/api/wallpaper?offset=${offset}&size=${size}", listener)
            }

            fun sync(date: Date?, listener: VolleyListener<JSONObject>): AuthJsonObjectRequest {
                val request = JSONObject()
                request.put("synckey", date?.syncFormat() ?: "1970-01-01 00:00:01")
                return AuthJsonObjectRequest(Method.POST, "${Api.HOST}/api/wallpaper/sync", request, listener)
            }

            fun localUpdate(jsonRequest: JSONObject, listener: VolleyListener<JSONObject>): AuthJsonObjectRequest {
                return AuthJsonObjectRequest(Method.PUT, "${Api.HOST}/api/local/wallpaper/update", jsonRequest, listener)
            }
        }
    }


    class WallpaperList(url: String, listener: VolleyListener<JSONObject>) :
    //
    //ttp://localhost:5001/
            AuthJsonObjectRequest(Method.GET, url, null, listener) {
        //http://appszum.com//api/wallpaper/category/download
        //http://localhost:5001/api/wallpaper/category/select
        // {"items": [{"key": "aves1mpyx0o","catkey": "0i512dgsztk", "views": true}]}

        companion object {


            fun list(category: Category, offset: Int, listener: VolleyListener<JSONObject>): WallpaperList {
                return WallpaperList("${Api.HOST}/api/wallpaper/category/${category.key}?size=30&offset=${offset}", listener)
            }

            fun allItems(category: Category, listener: VolleyListener<JSONObject>): WallpaperList {
                return WallpaperList("${Api.HOST}/api/wallpaper/category/${category.key}?size=${category.total}&offset=0", listener)
            }

            fun top(topColumn: TopColumn, listener: VolleyListener<JSONObject>): WallpaperList {
                return WallpaperList("${Api.HOST}/api/wallpaper/category/top/${topColumn}?top=100", listener)
            }

            private fun select(request: JSONObject, listener: VolleyListener<JSONObject>): AuthJsonObjectRequest {
                return AuthJsonObjectRequest(Method.POST, "${Api.HOST}/api/wallpaper/category/select", request, listener)
            }

            fun simillar(wallpaperInfo: WallpaperInfo, listener: VolleyListener<JSONObject>): AuthJsonObjectRequest {
                val request = JSONObject()
                request.put("catkey", wallpaperInfo.catkey)
                return AuthJsonObjectRequest(Method.POST, "${Api.HOST}/api/wallpaper/simillar", request, listener)
            }

            fun view(wallpaperInfo: WallpaperInfo, listener: VolleyListener<JSONObject>): AuthJsonObjectRequest {
                val array = JSONArray()
                val item = JSONObject()
                val request = JSONObject()
                item.put("key", wallpaperInfo.key)

                item.put("catkey", wallpaperInfo.catkey)
                item.put(TopColumn.views.name, true)
                array.put(item)
                request.put("items", array)
                return select(request, listener)
            }

            fun likes(wallpaperInfo: WallpaperInfo, like: Boolean?, dislike: Boolean?, listener: VolleyListener<JSONObject>): AuthJsonObjectRequest {
                val array = JSONArray()
                val item = JSONObject()
                val request = JSONObject()
                item.put("key", wallpaperInfo.key)

                item.put("catkey", wallpaperInfo.catkey)
                if (like != null) {
                    item.put(TopColumn.likes.name, like)
                }
                if (dislike != null) {
                    item.put(TopColumn.dislike.name, dislike)
                }
                array.put(item)
                request.put("items", array)
                return select(request, listener)
            }
        }

        enum class TopColumn {
            views, download, likes, dislike, favorite, downloaded, all;

            fun intent(intent: Intent): Intent {
                intent.putExtra("TopColumn", this)
                return intent
            }

            val bundle: Bundle
                get() {
                    return Bundle().apply {
                        putSerializable("TopColumn", this@TopColumn)
                    }
                }

            val isFavorite: Boolean get() = this == favorite

            val isDownloaded: Boolean get() = this == downloaded

            val isAll: Boolean get() = this == all

            val fromDb: Boolean get() = isDownloaded || isFavorite || isAll

            val title: String get() = if (fromDb) name.toUpperCase() else "TOP 100 $name ".toUpperCase()

            fun get(wallpaperInfoDao: WallpaperInfoDao): List<WallpaperInfo> {
                if (isFavorite) {
                    return wallpaperInfoDao.allFavorite()
                }

                if (isDownloaded) {
                    return wallpaperInfoDao.allDownloaded()
                }

                return wallpaperInfoDao.getAll()
            }


            companion object {
                fun get(bundle: Bundle): TopColumn? {
                    return if (bundle.containsKey("TopColumn")) bundle.getSerializable("TopColumn") as TopColumn else null
                }

                fun get(intent: Intent): TopColumn? {
                    return if (intent.hasExtra("TopColumn")) intent.getSerializableExtra("TopColumn") as TopColumn else null
                }
            }
        }
    }


    class SaveInfoApi(category: Category, request: JSONObject, listener: VolleyListener<JSONObject>) :
            AuthJsonObjectRequest(Method.POST, "${Api.HOST}/api/local/wallpaper/category/${category.key}", request, listener) {

    }

    class ImageRequest {

        var url: String

        var width: Int
        var height: Int


        private constructor(url: String, width: Int, height: Int = 0) {
            this.url = url

            this.width = width
            this.height = 0
        }

        fun call(context: Context, listener: Response.Listener<Bitmap>) {
            //SingletonRequestQueue.getInstance(getApplicationContext()).getRequestQueue();
//            if (imageLoader != null ){
//                if (imageLoader.isCached(url)){
//                    imageLoader?.get(url, object : ImageLoader.ImageListener{
//                        override fun onResponse(response: ImageLoader.ImageContainer?, isImmediate: Boolean) {
//                            if(response == null){return}
//                           listener.onResponse(response.bitmap)
//                        }
//
//                        override fun onErrorResponse(error: VolleyError?) {
//                            //listener
//                        }
//                    }, width, height)
//                    return
//                   // listener.onResponse(imageLoader.)
//                }
//            }
            if (AuthJsonObjectRequest.queue == null) {
                AuthJsonObjectRequest.queue = Volley.newRequestQueue(context)
            }

            if (imageLoader == null) {
                setImageLoader(AuthJsonObjectRequest.queue!!)
            }
            AuthJsonObjectRequest.queue?.add(request(listener))

        }

        fun call(imageView: NetworkImageView) {
            if (AuthJsonObjectRequest.queue == null) {
                AuthJsonObjectRequest.queue = Volley.newRequestQueue(imageView.context)
            }
            if (imageLoader == null) {
                setImageLoader(AuthJsonObjectRequest.queue!!)
            }
            imageView.setImageUrl(url, imageLoader)
        }

        private fun request(listener: Response.Listener<Bitmap>): com.android.volley.toolbox.ImageRequest {
            return com.android.volley.toolbox.ImageRequest(url, listener, width, height, ImageView.ScaleType.FIT_CENTER, null, null)
        }


        companion object {
            var imageLoader: ImageLoader? = null
            var imageCache = ImageCache()

            private fun setImageLoader(queue: RequestQueue) {
                if (imageLoader != null) {
                    return
                }
                imageLoader = ImageLoader(queue, imageCache)
            }


            fun thumb(category: Category): ImageRequest {
                //"https://firebasestorage.googleapis.com/v0/b/k-wallpaper-33b27.appspot.com/o/category%2F46790.jpg?alt=media&token=3d647f32-a901-446f-ab00-2ec4e8882efc"
                //return ImageRequest("https://firebasestorage.googleapis.com/v0/b/k-wallpaper-33b27.appspot.com/o/category%2F46790.jpg?alt=media&token=3d647f32-a901-446f-ab00-2ec4e8882efc", 512)
                return ImageRequest(category.thumbnails, 512)
            }

            fun thumb(uri: String): ImageRequest {
                //"https://firebasestorage.googleapis.com/v0/b/k-wallpaper-33b27.appspot.com/o/category%2F46790.jpg?alt=media&token=3d647f32-a901-446f-ab00-2ec4e8882efc"
                //return ImageRequest("https://firebasestorage.googleapis.com/v0/b/k-wallpaper-33b27.appspot.com/o/category%2F46790.jpg?alt=media&token=3d647f32-a901-446f-ab00-2ec4e8882efc", 512)
                return ImageRequest(uri, 512)
            }

            fun thumb(wallpaperInfo: WallpaperInfo): ImageRequest {
                return ImageRequest("https://firebasestorage.googleapis.com/v0/b/k-wallpaper-33b27.appspot.com/o/category%2F46790.jpg?alt=media&token=3d647f32-a901-446f-ab00-2ec4e8882efc", 512)
            }

            fun image(wallpaperInfo: WallpaperInfo): ImageRequest {
                return ImageRequest(wallpaperInfo._4kUrl, wallpaperInfo.width, wallpaperInfo.height)
            }

        }


    }
}

open class AuthJsonObjectRequest(
        method: Int,
        url: String,
        jsonRequest: JSONObject?,
        listener: com.appszum.lib.api.Api.VolleyListener<JSONObject>
) : com.appszum.lib.api.AuthJsonObjectRequest(method, url, jsonRequest, listener) {
    companion object {
        var queue: RequestQueue?
            get() = com.appszum.lib.api.AuthJsonObjectRequest.queue
            set(value) {
                com.appszum.lib.api.AuthJsonObjectRequest.queue = value
            }
    }

    init {
        headers["key"] = BuildConfig.API_KEY
    }
}