package com.appszum.wallpaper.hd

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.Volley
import com.appszum.lib.AdminActivity
import com.appszum.lib.model.LoginUser
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.CategoryResponse
import com.appszum.wallpaper.hd.module.Category
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AdminActivity : AdminActivity() {
    //http://superheroeswallpapers.mrdroidproductions.xyz/images/small/
    //http://uhdwallpapers.mrdroidstudios.xyz/images/thumb/
    //http://superheroeswallpapers.mrdroidproductions.xyz/script/get_categories.php
    //http://superheroeswallpapers.mrdroidproductions.xyz/images/thumb/
    //http://uhdwallpapers.mrdroidstudios.xyz//script/get_categories.php
    var list: ArrayList<Category> = ArrayList();

    var fbUrls: Map<String, String> = HashMap();


    var storage = FirebaseStorage.getInstance().getReference("category").child("thumb")
    var database = FirebaseFirestore.getInstance().collection("category");
    var syncPath = FirebaseFirestore.getInstance().collection("sync").document("eBsYXDPihxIcHHzHk9Lb");
    //val syncKey = "eBsYXDPihxIcHHzHk9Lb";


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        btn2.setText("Upload Bitmap")
    }


    var mAuth = FirebaseAuth.getInstance();

    override fun save(view: View) {
        showProgressAlert(getString(R.string.loading))
        if (!list.isEmpty()) {
            save(list[position])
            return
        }
    }

    override fun saveBitmap(view: View) {
        showProgressAlert(getString(R.string.loading))
        if (!list.isEmpty()) {
            saveBitmap(list[position])
            return
        }
    }


    fun categories(action: (CategoryResponse) -> Boolean) {
        Api.CategoryList.list(0, 100, object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
            override fun onResponse(response: JSONObject) {

                Log.d("response", response.toString());
                val r: CategoryResponse = Gson().fromJson(response.toString(), CategoryResponse::class.java)
                //list = r.response.records;
                val result = r.response
                if (result == null) {
                    return
                }
                log("\n\nfound: ${result.found}")
                log(" count: ${result.records.count()}")
                action(r)
            }

            override fun onErrorResponse(error: VolleyError) {
                dismissProgressAlert()
                Log.d("response", error.toString());
            }
        }).call(this)
    }

    private fun saveBitmap(category: Category): Boolean {
        if (category.fImage != null) {
            val next = this@AdminActivity.next
            if (next == null) {
                dismissProgressAlert()
                return true
            }
            saveBitmap(next)
            return true
        }
        val firbaseImage = storage.child("${category.key}.jpg")
        val url = firbaseImage.path

        showProgressAlert("fetching ${category.name} (${category.key}) image...")
        log("fetching ${category.name} (${category.key}) image...")
        Api.ImageRequest.thumb(category).call(this, object : Response.Listener<Bitmap> {
            override fun onResponse(response: Bitmap?) {
                log("image: ${response}")
                if (response == null) {
                    dismissProgressAlert()
                    return
                }
                showProgressAlert("saving ${category.name} (${category.key}) image...")
                log("saving ${category.name} (${category.key}) image...")

                val baos = ByteArrayOutputStream()
                response.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                val uploadTask = firbaseImage.putBytes(data)
                uploadTask.addOnFailureListener { exception ->
                    // Handle unsuccessful uploads

                    log("failed: ${exception}")
                    dismissProgressAlert()
                }.addOnProgressListener { snapshot ->
                    val progress = 100.0 * snapshot.getBytesTransferred() / snapshot.getTotalByteCount()
                    showProgressAlert("Uploaded " + progress.toInt() + "%...")
                }.addOnSuccessListener { snapshot ->
                    log("success: ${snapshot}")
                    category.fImage = snapshot.storage.path

                    database.document(category.key).set(category.map)

                    val next = this@AdminActivity.next
                    if (next == null) {
                        dismissProgressAlert()
                        return@addOnSuccessListener
                    }
                    saveBitmap(next)
                }
            }
        })
        return true
    }

    fun loadCategory() {
        list.clear()
        database.whereEqualTo("active", true).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result?.documents?.forEach { snapshot: DocumentSnapshot? ->
                    val data = snapshot?.data
                    if (data == null) {
                        return@forEach
                    }
                    val json = JSONObject()
                    data.keys.forEach { key: String? ->
                        if (key != null) {
                            json.put(key, data[key])
                        }
                    }
                    val category = Gson().fromJson(json.toString(), Category::class.java)
                    list.add(category)
                    print("${data} => document.data()")
                    print("${snapshot} => document.data()")
                }
            }
            val count = list.count()
            btn2.isEnabled = count > 0
            log("found ${count}")
        }

    }

    override fun onResume() {
        super.onResume()
        val currentUser = mAuth.currentUser
        if (currentUser == null) {
            startActivity(Intent(this, LoginActivity::class.java))
            return
        }
        log("user: ${LoginUser(currentUser)}")
        //database.whereEqualTo("")

        loadCategory()
        syncPath.get().addOnSuccessListener { documentSnapshot ->
            val count: Long = documentSnapshot["count"] as Long
            val last_update: Date = documentSnapshot["last_update"] as Date
            log("---- storm --")
            log("count: ${count}")
            log("last_update: ${last_update}")

            categories { response ->
                val result = response.response
                if (result == null) {
                    return@categories true
                }
                if (count < result.records.count()) {
                    result.records.forEach { category: Category ->
                        database.document(category.key).set(category.map).addOnSuccessListener { void ->
                            log("DocumentSnapshot added with ID: ${void}")
                        }.addOnFailureListener { exception ->
                            log("Error adding document")
                        }
                    }
                    val map = HashMap<String, Any>()
                    map.put("count", result.records.count())
                    map.put("last_update", Date())
                    syncPath.set(map)
                }

                true
            }
        }
    }


    val next: Category?
        get() {
            position++;
            if (position < list.count()) {
                return list[position];
            }
            return null
        }


    fun save(category: Category): Boolean {
        if (position >= list.count()) {
            dismissProgressAlert()
            return true
        }
        textView.text = "${textView.text}\n\n\n ${category.name} \ntotal: ${category.total}" +
                showProgressAlert("${position + 1}/${list.count()}  ${category.name} saving..")
        val request = JSONObject()
        request.put("insert", true)
        Volley.newRequestQueue(this).add(Api.SaveInfoApi(category, request, object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
            override fun onResponse(response: JSONObject) {
                var res = response.getJSONObject("response")
                for (key in arrayListOf("newitems", "inserted", "records")) {
                    var count: Int? = (res.get(key) as? JSONArray)?.length()
                    textView.text = "${textView.text}\n" +
                            "\n${key}: ${count}"
                }

                Log.d("response", response.toString());
                Log.d("saved", category.toString());

                val next = this@AdminActivity.next
                if (next == null) {
                    dismissProgressAlert()
                    return
                }
                save(next)
            }

            override fun onErrorResponse(error: VolleyError) {
                Log.d("response", error.toString());
                dismissProgressAlert()
            }
        }))
        return true
    }

}

