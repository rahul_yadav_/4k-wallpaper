package com.appszum.wallpaper.hd.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.VolleyError
import com.appszum.lib.haveInternetConnection
import com.appszum.lib.load
import com.appszum.lib.settings.SortingSetting
import com.appszum.lib.task.DbWorkerThread
import com.appszum.lib.task.TaskHandler
import com.appszum.lib.utils.ImageCache
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.adapter.CategoryAdapter
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.CategoryResponse
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperDatabase
import com.appszum.wallpaper.hd.task.CategoryImageLoad
import com.google.android.gms.ads.AdView
import com.google.gson.Gson
import org.json.JSONObject


class CategoryFragment : Fragment(), com.appszum.lib.api.Api.VolleyListener<JSONObject>, TaskHandler {

    private var type = 1

    private var listener: CategoryAdapter.AttachListener? = null

    lateinit var categoryAdapter: CategoryAdapter

    var found: Int = -1

    var db: WallpaperDatabase? = null
    var imageCache: ImageCache? = null

    private lateinit var mDbWorkerThread: DbWorkerThread

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        db = WallpaperDatabase.getInstance(context!!)

        arguments?.let {
            type = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    var filter: String?
        get() = categoryAdapter.filter
        set(value) {
            categoryAdapter.filter = value
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.category_list_view, container, false)

        categoryAdapter = CategoryAdapter(context!!, listener = listener?.listener)
        if (type == 0) {
            this.categoryAdapter.setting = SortingSetting(context!!)
        }
        if (db != null) {
            categoryAdapter.imageLoadListener = CategoryImageLoad(db!!, imageCache)
        }

        val recyclerView: RecyclerView = view.findViewById(R.id.list)
        with(recyclerView) {
            //layoutManager = LinearLayoutManager(context)
            layoutManager = GridLayoutManager(context, 2) as RecyclerView.LayoutManager?
            (layoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (!categoryAdapter.isAddView(position)) 1 else 2
                }
            }
            adapter = categoryAdapter
        }

        loadCategory()
        view.findViewById<AdView>(R.id.adViewBottom).load()
        return view
    }

    // val db = FirebaseFirestore.getInstance();

    //var database = db.collection("category");

    private fun callApi() {
        if (!context!!.haveInternetConnection() || type == 0) {
            fetchDataFromDb()
            return
        }
        if (type == 1) {
            Api.CategoryList.top(listener = this).call(this.context!!)
            return
        }
    }


    override fun ui(action: () -> Unit) {
        mDbWorkerThread.ui(action)
    }

    override fun task(action: () -> Unit) {
        mDbWorkerThread.task(action)
    }

    private fun fetchDataFromDb() {
        task {
            val list =
                    db?.categoryDao()?.getAll()
            ui {
                if (list != null) {
                    categoryAdapter.notifyDataChanged(list)
                }
            }
        }
    }

    private fun fetchDataFromDb(categories: ArrayList<Category>) {
        task {

            val list: ArrayList<Category> = arrayListOf()
            categories.forEach { category: Category ->
                val info =
                        db?.categoryDao()?.info(category.key)
                if (info != null) {
                    list.add(info)
                }
            }

            ui {
                categoryAdapter.notifyDataChanged(list)
            }
        }
    }


    fun loadCategory() {
        categoryAdapter.clearList()
        callApi()
    }


    override fun onResponse(response: JSONObject) {
        Log.d("response", response.toString())
        val r: CategoryResponse = Gson().fromJson(response.toString(), CategoryResponse::class.java)
        val result = r.response ?: return
        found = result.found
        fetchDataFromDb(result.records)
    }

    override fun onErrorResponse(error: VolleyError) {
        // alert.dismiss()
        Log.d("response", error.toString())
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroy() {
        mDbWorkerThread.quit()
        super.onDestroy()
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(listener: CategoryAdapter.AttachListener, columnCount: Int) =
                CategoryFragment().apply {
                    this.listener = listener
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
