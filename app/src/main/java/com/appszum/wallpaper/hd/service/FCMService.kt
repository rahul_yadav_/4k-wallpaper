package com.appszum.wallpaper.hd.service

import android.graphics.Bitmap
import com.android.volley.VolleyError
import com.appszum.lib.Notifications
import com.appszum.lib.fcm.FcmIntentService
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.WallpaperListActivity
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import org.json.JSONObject

class FCMService : FcmIntentService() {


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        notifications = Notifications(application as com.appszum.wallpaper.hd.Application, R.mipmap.ic_launcher_round)
        super.onMessageReceived(remoteMessage)
    }

    override fun getAlertMessage(locKeyVal: String): String {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return ""
    }

    override fun getBadgeMessage(badge: Int): String {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return ""
    }


    override fun notificationReceived(jsonObject: JSONObject, notificationId: Int) {
        if (jsonObject.has("category")) {
            notificationReceived(Gson().fromJson(jsonObject.toString(), Category::class.java), notificationId)
            return
        }

        if (jsonObject.has("wallpaper")) {
            notificationReceived(Gson().fromJson(jsonObject.toString(), WallpaperInfo::class.java), notificationId)
            return
        }
        super.notificationReceived(jsonObject, notificationId)
    }


    //    {
//        "name": "SUPERMAN",
//        "key": "0i512dgsztk",
//        "total": 150,
//        "path": "wallpaper/UHD/superman",
//        "icon": "image?path=wallpaper/UHD/superman/superman1.jpg"
//    }
    private fun notificationReceived(category: Category, notificationId: Int) {

        Api.ImageRequest.thumb(category).call(application, object : com.appszum.lib.api.Api.VolleyListener<Bitmap> {
            override fun onResponse(response: Bitmap) {
                notifications?.notify(category.name, notificationIntent = WallpaperListActivity.intent(application, category), message = "${category.name} 4K wallpaper has been released", bitmap = response, notificationId = notificationId)
            }

            override fun onErrorResponse(error: VolleyError) {

                notifications?.notify(category.name, notificationIntent = WallpaperListActivity.intent(application, category), message = "${category.name} 4K wallpaper has been released", notificationId = notificationId)
            }
        })

        // super.notificationReceived(jsonObject, notificationId)
    }


    //    {
//        "name": "superman99",
//        "key": "qtldp0ge2y0",
//        "catkey": "0i512dgsztk",
//        "size": 484155,
//        "download": 0,
//        "views": 2,
//        "likes": 0,
//        "dislikes": 0,
//        "width": 2160,
//        "height": 3840,
//        "icon": "image?path=wallpaper/UHD/superman/superman99.jpg&width=250",
//        "image": "image?path=wallpaper/UHD/superman/superman99.jpg"
//    }
    private fun notificationReceived(wallpaperInfo: WallpaperInfo, notificationId: Int) {

        // super.notificationReceived(jsonObject, notificationId)
    }


}