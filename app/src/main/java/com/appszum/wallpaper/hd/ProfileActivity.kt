package com.appszum.wallpaper.hd

import android.os.Bundle
import android.view.View
import com.appszum.lib.model.LoginUser


class ProfileActivity : com.appszum.lib.ProfileActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun facebookLinkOrUnLink(view: View) {
        super.facebookLinkOrUnLink(view)
    }

    override fun twitterLinkOrUnlink(view: View) {
        super.twitterLinkOrUnlink(view)
    }

    override fun logout(view: View) {
        super.logout(view)
    }

    override fun display(user: LoginUser) {
        super.display(user)
    }
}
