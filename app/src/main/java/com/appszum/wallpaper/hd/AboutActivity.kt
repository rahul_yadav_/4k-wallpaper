package com.appszum.wallpaper.hd

import com.appszum.lib.FirebaseRemoteConfig


class AboutActivity : com.appszum.lib.AboutActivity() {
    override val host: String
        get() = FirebaseRemoteConfig.HOST
    override val appId: String
        get() = FirebaseRemoteConfig.APP_ID
    override val name: String
        get() = "${BuildConfig.APP_NAME} V.${BuildConfig.VERSION_NAME}"
    override val icon: Int
        get() = R.drawable.web_hi_res_512

//    private val host: String get() = FirebaseRemoteConfig.HOST
//    private val appId: String get() = FirebaseRemoteConfig.APP_ID
//
//    val name: String
//        get() = "${BuildConfig.APP_NAME} V.${BuildConfig.VERSION_NAME}"


}