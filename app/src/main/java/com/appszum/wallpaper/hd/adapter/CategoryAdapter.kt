package com.appszum.wallpaper.hd.adapter


import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.appszum.lib.adapter.RecyclerViewSearchAdapter
import com.appszum.lib.listener.RecyclerViewListener
import com.appszum.lib.listener.ad.FBAdListener
import com.appszum.lib.settings.SortingSetting
import com.appszum.lib.task.ImageLoadListener
import com.appszum.lib.view.RecycleViewHolder
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.module.Category
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_item.view.*
import java.util.*

class CategoryAdapter(context: Context, @LayoutRes val itemLayout: Int = R.layout.category_item, listener: RecyclerViewListener<Category>?) : RecyclerViewSearchAdapter<RecycleViewHolder<Category>, Category>(listener) {

    interface AttachListener {
        var listener: RecyclerViewListener<Category>
    }

    init {
        adListener = FBAdListener(context, "757735714253412_2430962503597383") { notifyDataChanged() }
        //adListener = AdMobAdListener(context) { notifyDataChanged() }
    }

    var imageLoadListener: ImageLoadListener<Category>? = null

    var setting: SortingSetting? = null

    override fun sort() {
        super.sort()
        val setting = this.setting
        if (setting != null) {
            val ascending = setting.ascending
            when (setting.sortBy) {
                0 -> {
                    Collections.sort(list, if (ascending) Category.NameAscending else Category.NameDescending)
                }
                1 -> {
                    Collections.sort(list, if (ascending) Category.TotalAscending else Category.TotalDescending)
                }

                2 -> {
                    Collections.sort(list, if (ascending) Category.DateAscending else Category.DateDescending)
                }
            }
        }
    }

    override fun isValid(item: Category, filter: String): Boolean {
        return item.name.toLowerCase().contains(filter) || filter.contains(item.name.toLowerCase())
    }

    override fun getItemView(parent: ViewGroup): RecycleViewHolder<Category> = CategoryViewHolder.get(itemLayout, parent, listener, imageLoadListener)
}


class CategoryViewHolder(itemView: View, recyclerListener: RecyclerViewListener<Category>?, var imageLoadListener: ImageLoadListener<Category>?) : RecycleViewHolder<Category>(itemView, recyclerListener) {

    companion object {
        fun get(@LayoutRes layout: Int, parent: ViewGroup, recyclerListener: RecyclerViewListener<Category>?, imageLoadListener: ImageLoadListener<Category>?): CategoryViewHolder {
            return CategoryViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false), recyclerListener, imageLoadListener)
        }
    }

    private val mContentView: TextView = itemView.content
    private val icon: ImageView = itemView.icon
    override fun setItem(value: Category?) {
        super.setItem(value)
        if (value != null) {
            mContentView.text = value.name
            if (imageLoadListener == null) {
                loadImage(value)
                return
            }
            imageLoadListener?.load(value) { bitmap ->
                if (bitmap != null) {
                    icon.setImageBitmap(bitmap)
                    return@load
                }
                loadImage(value)
                return@load
            }
        }

    }

    fun loadImage(category: Category) {
        Picasso.get()
                .load(category.thumbnails)
                .placeholder(R.drawable.loading_text)
                .into(icon, object : Callback {
                    override fun onSuccess() {
                        val image = (icon.drawable as BitmapDrawable).bitmap
                        if (image != null) {
                            imageLoadListener?.save(category, image)
                        }
                    }

                    override fun onError(e: java.lang.Exception?) {

                    }
                })
    }

    override fun toString(): String {
        return super.toString() + " '" + mContentView.text + "'"
    }
}


//open class RecyclerViewHolder(val view: View): RecyclerView.ViewHolder(view){
//
//}


