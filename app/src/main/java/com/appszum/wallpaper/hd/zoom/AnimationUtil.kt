package com.appszum.wallpaper.hd.zoom

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import android.view.animation.Interpolator
import android.view.animation.TranslateAnimation


class AnimationUtil : AnimationListener {

    private val animation: Animation
    var animationEndListener: OnAnimationEndListener? = null
    var animationStartListener: OnAnimationStartListener? = null
    var animationRepeatListener: OnAnimationRepeatListener? = null

    var startOffset: Long
        get() = animation.startOffset
        set(value) {
            animation.startOffset = value
        }

    var interpolator: Interpolator
        get() = animation.interpolator
        set(value) {
            animation.interpolator = value
        }

    var duration: Long
        get() = animation.duration
        set(value) {
            animation.duration = value
        }

    var fillAfter: Boolean
        get() = animation.fillAfter
        set(value) {
            animation.fillAfter = value
        }

    constructor(context: Context, resId: Int) {
        this.animation = AnimationUtils.loadAnimation(context, resId)
        this.animation.setAnimationListener(this)
    }

    constructor(fromXDelta: Float, toXDelta: Float, fromYDelta: Float,
                toYDelta: Float) {
        animation = TranslateAnimation(fromXDelta, toXDelta, fromYDelta,
                toYDelta)
    }

    fun startAnimation(view: View) {
        view.startAnimation(animation)
    }


    fun setAnimationListener(animationListener: AnimationListener) {
        animation.setAnimationListener(animationListener)
    }

    override fun onAnimationStart(animation: Animation) {
        this.animationStartListener?.onAnimationStart(animation)
    }

    override fun onAnimationEnd(animation: Animation) {
        this.animationEndListener?.onAnimationEnd(animation)
    }

    override fun onAnimationRepeat(animation: Animation) {
        this.animationRepeatListener?.onAnimationRepeat(animation)
    }

    interface OnAnimationEndListener {
        fun onAnimationEnd(animation: Animation)
    }

    interface OnAnimationStartListener {
        fun onAnimationStart(animation: Animation)
    }

    interface OnAnimationRepeatListener {
        fun onAnimationRepeat(animation: Animation)
    }

    companion object {

        fun startAnimation(resId: Int, view: View) {
            view.setBackgroundResource(resId)
            (view.background as AnimationDrawable).start()
        }
    }

}
