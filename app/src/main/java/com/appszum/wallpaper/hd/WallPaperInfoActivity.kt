package com.appszum.wallpaper.hd

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.android.volley.VolleyError
import com.appszum.lib.blur
import com.appszum.lib.error
import com.appszum.lib.listener.RecyclerListener
import com.appszum.lib.listener.RecyclerViewListener
import com.appszum.lib.load
import com.appszum.lib.utils.Utility
import com.appszum.lib.view.RecycleViewHolder
import com.appszum.wallpaper.hd.adapter.CategoryAdapter
import com.appszum.wallpaper.hd.adapter.WallpaperAdapter
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.WallpaperResponse
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.appszum.wallpaper.hd.task.CategoryImageLoad
import com.google.gson.Gson
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_scrolling.*
import org.json.JSONObject

class WallPaperInfoActivity : BaseInfoActivity() {

    lateinit var categoryAdapter: CategoryAdapter
    lateinit var wallpaperAdapter: WallpaperAdapter

    private var categoryListener: RecyclerViewListener<Category> = object : RecyclerListener<Category>() {
        override fun onItemClick(holder: RecycleViewHolder<Category>, value: Category) {
            super.onItemClick(holder, value)

            val options = ActivityOptions.makeSceneTransitionAnimation(this@WallPaperInfoActivity, holder.itemView, getString(R.string.app_home))
            startActivity(WallpaperListActivity.intent(this@WallPaperInfoActivity, value), options.toBundle())
            //overridePendingTransition(R.anim.fade_in, R.anim.zoom_out)
        }
    }

    private val wallpaperListener: RecyclerViewListener<WallpaperInfo> = object : RecyclerListener<WallpaperInfo>() {
        override fun onItemClick(holder: RecycleViewHolder<WallpaperInfo>, value: WallpaperInfo) {
            super.onItemClick(holder, value)
            val options = ActivityOptions.makeSceneTransitionAnimation(this@WallPaperInfoActivity, holder.itemView, getString(R.string.app_home))
            startActivity(WallPaperInfoActivity.intent(this@WallPaperInfoActivity, value), options.toBundle())
        }
    }

    companion object {
        fun intent(context: Context, wallpaperInfo: WallpaperInfo): Intent {
            return wallpaperInfo.intent(Intent(context, WallPaperInfoActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(toolbar)
        categoryAdapter = CategoryAdapter(this, R.layout.category_item1, categoryListener)
        categoryAdapter.imageLoadListener = CategoryImageLoad(db)
        wallpaperAdapter = WallpaperAdapter(this, R.layout.wallpaper_item2, wallpaperListener)
        with(rvCategories) {
            //layoutManager = LinearLayoutManager(context)
            layoutManager = LinearLayoutManager(this@WallPaperInfoActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = categoryAdapter
        }

        with(rvWallpapers) {
            //layoutManager = LinearLayoutManager(context)
            layoutManager = LinearLayoutManager(this@WallPaperInfoActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = wallpaperAdapter
        }

        adView1.load()
    }

    override fun onSelect(wallpaperInfo: WallpaperInfo) {
        super.onSelect(wallpaperInfo)
        fabLike.setIcon(if (wallpaperInfo.liked) R.drawable.like_on else R.drawable.like_off)
        fabDislike.setIcon(if (wallpaperInfo.disliked) R.drawable.dislike_on else R.drawable.dislike_off)
        invalidateOptionsMenu()
    }

    override var wallpaperInfo: WallpaperInfo?
        get() = super.wallpaperInfo
        set(wallpaperInfo) {
            super.wallpaperInfo = wallpaperInfo
            if (wallpaperInfo != null) {
                tvSize.text = wallpaperInfo.sizeInfo
                tvDownload.text = Utility.count(wallpaperInfo.download)
                tvLike.text = Utility.count(wallpaperInfo.likes)
                tvView.text = Utility.count(wallpaperInfo.views)
                tvInfo.text = "${wallpaperInfo.width}x${wallpaperInfo.height}"
                Picasso.get()
                        .load(wallpaperInfo.imagePath)
                        .placeholder(R.drawable.loading_text)
                        .into(ivIcon, object : Callback {
                            override fun onSuccess() {
                                ivIcon.scaleType = ImageView.ScaleType.FIT_XY
                                val _bitmap = (ivIcon.drawable as BitmapDrawable).bitmap
                                if (_bitmap != null) {
                                    ivBg.setImageBitmap(_bitmap.blur(this@WallPaperInfoActivity))
                                    ivBg.scaleType = ImageView.ScaleType.FIT_XY
                                }
                            }

                            override fun onError(e: java.lang.Exception?) {

                            }
                        })

                showProgressAlert()

                Api.WallpaperList.simillar(wallpaperInfo, listener = object : com.appszum.lib.api.Api.VolleyListener<JSONObject> {
                    override fun onErrorResponse(error: VolleyError) {
                        application.error(error)
                    }

                    override fun onResponse(response: JSONObject) {
                        print(response)
                        val r: WallpaperResponse = Gson().fromJson(response.toString(), WallpaperResponse::class.java)
                        val result = r.response ?: return

                        categoryAdapter.notifyDataChanged(result.categories.filter { category -> category.key != wallpaperInfo.catkey })
                        wallpaperAdapter.notifyDataChanged(result.records.filter { wal -> wal.key != wallpaperInfo.key })

                        dismissProgressAlert()
                    }
                }).call(this)
            }
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.info_menu, menu)

        menu.findItem(R.id.favorite).setIcon(if (wallpaperInfo?.favorite == true) R.drawable.favorite_on else R.drawable.favorite_off)
        menu.findItem(R.id.download).setIcon(if (wallpaperInfo?.dowloaded == true) R.drawable.download_on else R.drawable.download_off)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item != null) {
            when (item.itemId) {
                R.id.download -> {
                    download(wallpaperInfo!!)
                    invalidateOptionsMenu()
                    return true
                }
//                R.id.save -> {
//                    //save(wallpaperInfo!!)
//                    return true
//                }

//                R.id.share -> {
//                    share(View(this))
//                    return true
//                }

                R.id.fbShare -> {
                    shareOnFacebook()
                    return true
                }

                R.id.favorite -> {
                    favorite(wallpaperInfo!!)
                    invalidateOptionsMenu()
                    return true
                }
//            R.id.navigation_notifications -> {
//                message.setText(R.string.title_notifications)
//                return@OnNavigationItemSelectedListener true
//            }
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun info(wallpaperInfo: WallpaperInfo) {

    }


    override fun like(view: View) {
        super.like(view)
    }

    override fun dislike(view: View) {
        super.dislike(view)
    }

    override fun setWallpaper(view: View) {
        super.setWallpaper(view)
    }

    override fun categoryFound(category: Category) {
        super.categoryFound(category)
        tvCatName.text = category.name
        Picasso.get().load(category.thumbnails).placeholder(R.drawable.loading_text).into(ivCatIcon)
        lCategory.setOnClickListener { startActivity(WallpaperListActivity.intent(this, category)) }
    }

    override val selected: WallpaperInfo
        get() = wallpaperInfo!!
}
