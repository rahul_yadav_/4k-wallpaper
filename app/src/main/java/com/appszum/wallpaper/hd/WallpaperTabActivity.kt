package com.appszum.wallpaper.hd

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.MenuItem
import com.appszum.lib.BaseActivity
import com.appszum.lib.utils.ImageCache
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.fragment.WallpaperTabFragment
import kotlinx.android.synthetic.main.activity_wallpaper_bottom_tabs.*

class WallpaperTabActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    val imageCache = ImageCache()
    var fragment: WallpaperTabFragment? = null

    val navigationView: BottomNavigationView
        get() = findViewById(R.id.navigation)

    var top: Api.WallpaperList.TopColumn? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallpaper_bottom_tabs)
        setSupportActionBar(toolbar)
        title = BuildConfig.APP_NAME
        navigationView.setOnNavigationItemSelectedListener(this)
        //reload()
        replace(WallpaperTabFragment.newInstance(imageCache))
        top = Api.WallpaperList.TopColumn.get(intent)
        navigationView.menu.getItem(1).isVisible = top == null
    }

    fun replace(fragment: WallpaperTabFragment) {
        // First get FragmentManager object.
        this.fragment = fragment

        val fragmentManager = this.supportFragmentManager

        // Begin Fragment transaction.
        val fragmentTransaction = fragmentManager.beginTransaction()

        // Replace the layout holder with the required Fragment object.
        fragmentTransaction.replace(R.id.container, fragment)

        // Commit the Fragment replace action.
        fragmentTransaction.commit()
        invalidateOptionsMenu()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.category -> {
                replace(WallpaperTabFragment.newInstance(imageCache))
            }
            R.id.saved_wallpaper -> {
                replace(WallpaperTabFragment.newInstance(imageCache, Api.WallpaperList.TopColumn.downloaded, Api.WallpaperList.TopColumn.favorite))
            }
            R.id.top_100_wallpapers -> {
                replace(WallpaperTabFragment.newInstance(imageCache, Api.WallpaperList.TopColumn.download, Api.WallpaperList.TopColumn.likes, Api.WallpaperList.TopColumn.views))
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 100 && resultCode == 100 && data != null) {
            setResult(resultCode, data)
            finish()
        }
    }

}