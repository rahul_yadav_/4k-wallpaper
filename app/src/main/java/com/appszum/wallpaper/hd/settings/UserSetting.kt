package com.appszum.wallpaper.hd.settings

import android.content.Context
import com.appszum.wallpaper.hd.module.WallpaperInfo
import org.json.JSONObject

class UserSetting(context: Context) : com.appszum.lib.settings.UserSetting(context) {

    fun like(wallpaperInfo: WallpaperInfo): JSONObject {
        val jsonObject = likes
        if (wallpaperInfo._like != 0) {
            jsonObject.put(wallpaperInfo.key, wallpaperInfo.liked)
        } else if (jsonObject.has(wallpaperInfo.key)) {
            jsonObject.remove(wallpaperInfo.key)
        }
        save("likes", jsonObject.toString())
        return jsonObject
    }
}