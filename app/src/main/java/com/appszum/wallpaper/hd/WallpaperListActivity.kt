package com.appszum.wallpaper.hd


import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appszum.lib.SingleFragmentActivity
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.fragment.WallpaperListFragment
import com.appszum.wallpaper.hd.module.Category


class WallpaperListActivity : SingleFragmentActivity() {

    companion object {
        fun intent(context: Context, category: Category): Intent {
            return category.intent(Intent(context, WallpaperListActivity::class.java))
        }

        fun intent(context: Context, top: Api.WallpaperList.TopColumn): Intent {
            return top.intent(Intent(context, WallpaperListActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val category = Category.get(intent)
        if (category != null) {
            replace(WallpaperListFragment.newInstance(category))
            title = category.name
            return
        }
        val top = Api.WallpaperList.TopColumn.get(intent)

        if (top != null) {
            replace(WallpaperListFragment.newInstance(top))
            title = top.title
            return
        }
        finish()
    }

}
