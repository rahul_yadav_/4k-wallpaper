package com.appszum.wallpaper.hd.fragment

import android.annotation.SuppressLint
import android.app.ActivityOptions
import android.app.WallpaperManager
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.VolleyError
import com.appszum.lib.dialog.AlertView
import com.appszum.lib.dismissProgressAlert
import com.appszum.lib.listener.RecyclerListener
import com.appszum.lib.listener.RecyclerViewListener
import com.appszum.lib.load
import com.appszum.lib.showProgressAlert
import com.appszum.lib.task.DbWorkerThread
import com.appszum.lib.task.DownloadListener
import com.appszum.lib.task.TaskHandler
import com.appszum.lib.toast
import com.appszum.lib.view.RecycleViewHolder
import com.appszum.wallpaper.hd.R
import com.appszum.wallpaper.hd.WallPaperInfoActivity
import com.appszum.wallpaper.hd.WallpaperPagerActivity
import com.appszum.wallpaper.hd.WallpaperTabActivity
import com.appszum.wallpaper.hd.adapter.WallpaperAdapter
import com.appszum.wallpaper.hd.api.Api
import com.appszum.wallpaper.hd.api.WallpaperResponse
import com.appszum.wallpaper.hd.dialog.DownloadDialog
import com.appszum.wallpaper.hd.module.Category
import com.appszum.wallpaper.hd.module.WallpaperDatabase
import com.appszum.wallpaper.hd.module.WallpaperInfo
import com.appszum.wallpaper.hd.task.BitmapDownloadTask
import com.appszum.wallpaper.hd.task.WallpaperImageLoad
import com.google.android.gms.ads.AdView
import com.google.gson.Gson
import org.json.JSONObject


class WallpaperListFragment : Fragment(), com.appszum.lib.api.Api.VolleyListener<JSONObject>, TaskHandler {

    var found: Int = -1;
    var category: Category? = null
    var top: Api.WallpaperList.TopColumn? = null

    lateinit var adapter: WallpaperAdapter

    private lateinit var mDbWorkerThread: DbWorkerThread

    var db: WallpaperDatabase? = null

    var imageLoad: WallpaperImageLoad? = null

    val listener: RecyclerViewListener<WallpaperInfo> = object : RecyclerListener<WallpaperInfo>() {
        override fun onItemClick(holder: RecycleViewHolder<WallpaperInfo>, value: WallpaperInfo) {
            super.onItemClick(holder, value)
            val context = this@WallpaperListFragment.context ?: return
            if (activity?.callingActivity != null) {
                activity?.setResult(100, value.intent(Intent()))
                activity?.finish()
                return
            }
            val options = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions.makeSceneTransitionAnimation(activity, holder.itemView, getString(R.string.app_home))
            } else {
                null
            }
            if (category != null) {
                startActivity(WallpaperPagerActivity.intent(context, category!!, value), options?.toBundle())
            } else if (top !== null) {
                startActivity(WallpaperPagerActivity.intent(context, top!!, value), options?.toBundle())
            }
        }

        override fun loadMore() {
            this@WallpaperListFragment.loadMore()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(category: Category) =
                WallpaperListFragment().apply {
                    arguments = category.bundle
                }

        @JvmStatic
        fun newInstance(top: Api.WallpaperList.TopColumn) =
                WallpaperListFragment().apply {
                    arguments = top.bundle
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDbWorkerThread = DbWorkerThread("WallpaperListFragment")
        db = WallpaperDatabase.getInstance(context!!)
        imageLoad = WallpaperImageLoad(db!!)
        arguments?.let {
            top = Api.WallpaperList.TopColumn.get(it)
            category = Category.get(it)
        }
    }


    @SuppressLint("RestrictedApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.content_wallpaper_list, container, false)
        val context = this@WallpaperListFragment.context ?: return null
        adapter = WallpaperAdapter(context, listener = listener, imageLoadListener = WallpaperImageLoad(db!!))

        val recyclerView: RecyclerView = view.findViewById(R.id.list)
        recyclerView.adapter = adapter
        with(recyclerView) {
            layoutManager = GridLayoutManager(context, 2)
            (layoutManager as GridLayoutManager).spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (!this@WallpaperListFragment.adapter.isAddView(position)) 1 else 2
                }
            }
        }

        view.findViewById<AdView>(R.id.adViewBottom).load()
        val fabAdd = view.findViewById<FloatingActionButton>(R.id.fabAdd)
        fabAdd.visibility = if (activity?.callingActivity == null && top?.fromDb == true) View.VISIBLE else View.GONE

        fabAdd.setOnClickListener {
            var intent = Intent(context, WallpaperTabActivity::class.java)
            if (top != null) {
                intent = top!!.intent(intent)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivityForResult(intent, 100, ActivityOptions.makeSceneTransitionAnimation(activity, it, getString(R.string.app_home)).toBundle())
            } else {
                startActivityForResult(intent, 100)
            }
        }
        loadWallpaper()
        return view
    }

    private fun callApi() {
        if (category != null) {
            Api.WallpaperList.list(category!!, adapter.itemCount, listener = this).call(context!!)
        } else if (top != null) {
            show(top!!)
        }
    }

    fun show(top: Api.WallpaperList.TopColumn) {
        if (!top.fromDb) {
            Api.WallpaperList.top(top, listener = this).call(context!!)
            return
        }

        task {
            with(adapter) {
                val dao = db?.wallpaperInfoDao()
                val list = if (dao != null) top.get(dao) else arrayListOf()
                this@WallpaperListFragment.ui {
                    notifyDataChanged(list)
                }
            }
        }
    }

    private fun loadMore() {
        if (top != null || (found != -1 && adapter.itemCount >= found)) {
            return
        }
        callApi()
    }

    private fun loadWallpaper() {
        adapter.clearList()
        callApi()
    }

    override fun onResponse(response: JSONObject) {
        Log.d("response", response.toString());
        val r: WallpaperResponse = Gson().fromJson(response.toString(), WallpaperResponse::class.java)
        val result = r.response ?: return
        found = result.found
        with(adapter) {
            notifyAddMore(result.records)
        }
    }

    override fun onErrorResponse(error: VolleyError) {
        Log.d("response", error.toString());
    }

    fun save(wallpaperInfo: WallpaperInfo) {
        wallpaperInfo.favorite = true
        task {
            val dao = db?.wallpaperInfoDao() ?: return@task
            if (wallpaperInfo.favorite) {
                wallpaperInfo.update(dao)
                toast(getString(R.string.favorite_added))
            } else {
                if (wallpaperInfo.dowloaded) {
                    wallpaperInfo.update(dao)
                } else {
                    dao.remove(wallpaperInfo.key, wallpaperInfo.catkey)
                }
                toast(getString(R.string.favorite_remove))
            }
            ui {
                show(top!!)
            }
        }
    }

    override fun task(action: () -> Unit) {
        mDbWorkerThread.task(action)
    }

    override fun ui(action: () -> Unit) {
        mDbWorkerThread.ui(action)
    }

    private fun setWallpaper(bitmap: Bitmap, which: Int, action: () -> Unit) {
        task {
            val wallpaperManager = WallpaperManager.getInstance(activity!!.applicationContext)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                var flags = WallpaperManager.FLAG_SYSTEM
                if (which == 1) {
                    flags = WallpaperManager.FLAG_LOCK
                } else if (which == 2) {
                    flags = WallpaperManager.FLAG_SYSTEM or WallpaperManager.FLAG_SYSTEM
                }
                wallpaperManager.setBitmap(bitmap, Rect(0, 0, bitmap.width, bitmap.height), false, flags)
            } else {
                wallpaperManager.setBitmap(bitmap)
            }
            ui {
                toast("Wallpaper set successfully!!")
                action()
            }
        }

    }

    fun load4KImage(wallpaperInfo: WallpaperInfo, action: (Bitmap?) -> Unit) {
        imageLoad?.load(wallpaperInfo) {
            if (it != null) {
                action(it)
                return@load
            }

            showProgressAlert("loading 4k image...")

            BitmapDownloadTask(wallpaperInfo, object : DownloadListener<Bitmap> {
                override fun success(result: Bitmap) {
                    action(result)
                    dismissProgressAlert()
                }

                override fun onProgressUpdate(progress: Float) {
                    showProgressAlert("${String.format("%.2f", progress)}% loading 4k image...")
                }

                override fun error() {
                    toast("problem in image loading!!")
                    action(null)
                    dismissProgressAlert()
                }
            }).execute()
        }
    }

    private fun setWallpaper(wallpaperInfo: WallpaperInfo) {
        AlertView.popUpDialog<String>(context!!, getString(R.string.set_wallpaper), arrayOf(getString(R.string.home_screen), getString(R.string.lock_screen), getString(R.string.home_and_lock_screen)), 0)
        { i: Int, _: String? ->
            load4KImage(wallpaperInfo) {
                if (it != null) {
                    showProgressAlert("setting wallpaper....")
                    setWallpaper(it, which = i) {
                        dismissProgressAlert()
                    }
                    return@load4KImage
                }
            }
        }

    }

    fun download(wallpaperInfo: WallpaperInfo, bitmap: Bitmap, onSave: (Boolean) -> Unit) {
        imageLoad?.save(wallpaperInfo, bitmap) {
            toast(getString(if (it) R.string.successfully_downloaded else R.string.problem_with_image_downloading))
            onSave(it)
        }
    }

    fun download(wallpaperInfo: WallpaperInfo) {
        DownloadDialog.show(context!!, wallpaperInfo, object : DownloadDialog.Listener {
            override fun info(wallpaperInfo: WallpaperInfo) {
                startActivity(WallPaperInfoActivity.intent(context!!, wallpaperInfo))
            }

            override fun setWallpaper(wallpaperInfo: WallpaperInfo) {
                this@WallpaperListFragment.setWallpaper(wallpaperInfo)
            }

            override fun save(wallpaperInfo: WallpaperInfo, bitmap: Bitmap, onSave: (Boolean) -> Unit) {
                download(wallpaperInfo, bitmap) {
                    onSave(it)
                    if (top?.isAll == true) {
                        save(wallpaperInfo)
                        return@download
                    }
                    show(top!!)
                }
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == 100 && data != null) {
            val wallpaperInfo = WallpaperInfo.get(data) ?: return
            if (top?.isDownloaded == true || top?.isAll == true) {
                download(wallpaperInfo)
            }

            if (top?.isFavorite == true) {

                save(wallpaperInfo)
            }
        }
    }

}
